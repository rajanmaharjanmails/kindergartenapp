module.exports = {
    "parser": "babel-eslint",
    "parserOptions": {
        "sourceType": "module",
        "ecmaVersion": 8,
        "ecmaFeatures": {
            "jsx": true,
            "experimentalObjectRestSpread": true
        }
    },
    "extends": [
        "standard",
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "rules": {
        "strict": 0,
        "semi": [2, "always"],
        "space-before-function-paren": ["error", "never"],
        "camelcase": [2, { "properties": "never" }],
        "react/display-name": [true, { "ignoreTranspilerName": true }],
        "react/prop-types": [true, { ignore: true, customValidators: true }],
        "indent": 0
    },
    "env": {
        "es6": true
    }
};