/**
 * Created by rajanmaharjan on 8/19/17.
 */

import {
  AppRegistry
} from 'react-native';

import KindergartenApp from './src/App';

AppRegistry.registerComponent('Kindergarten', () => KindergartenApp);
