# API

Note:
Date / Time will usually be provided in ISO8601 compatible format

**prefix**: /api/v1

POST /register

Request :
```json
{
  "email": "some@one.com",
  "password": "vErY_Secr8_passWORD",
  "code": "ASD2123L09SD",
}
```

POST /authenticate

Request:
```json
{
  "email": "some@one.com",
  "password": "vErY_Secr8_passWORD",
}
```
Response: Success
```json
{
  "access_token": "...",
  "refresh_token": "...",
  "issued_at": 123456789
}
```

GET /me
```json
{
  "name": "John Doe Sr.",
  "email": "john@doe.com",
  "contact_no": "9876543210",
  "address": "Somewhere, Somewhere",
  "joined_at": "ISO_DATE",
}
```

PUT /me

Request
```json
{
  "name": "John Doe Sr.",
  "contact_no": "9876543210",
  "address": "Somewhere, Somewhere",
}
```
Response: Success
```json
{
  "name": "John Doe Sr.",
  "email": "john@doe.com",
  "contact_no": "9876543210",
  "address": "Somewhere, Somewhere",
  "joined_at": "ISO_DATE",
}
```

POST /me/push_token

Request
```json
{
  "device_identifier": "Unique_device_code",
  "token": "FCM_TOKEN",
}
```

GET /me/students
```json
{
  "data": [
    {
      "id": 123,
      "name": "John Doe",
      "class": "Nursery",
      "photo_url": {
        "small": "http://...",
        "full": "http://..."
      }
    }
  ],
  "meta": {
    "total": 1,
    "per_page": 20,
    "page": 1,
  }
}
```

POST /me/students/subscribe

Request
```json
{
  "code": "ASDKLJKL123KLJK"
}
```
Response: Success
```json
{
  "id": 123,
  "name": "John Doe",
  "class": "Nursery",
  "photo_url": {
    "small": "http://...",
    "full": "http://..."
  }
}
```

POST /me/students/id/unsubscribe

Response: Success
```json
{
    "code": 200,
    "message": "Removed account successfully"
}
```


GET /student\_info/{registration\_code}
```json
{
  "code": "AS23L324132S",
  "name": "John Doe",
  "class": "Nursery",
  "photo_url": {
    "small": "http://..."
  }
}
```

GET /students/{id}/attendance_report?year={YY}&month={MM}
```json
{
  "year": 2017,
  "month": 12,
  "days": {
    "total": 31,
    "school": 25,
    "present": 21,
    "absent": 4,
    "late": 5
  },
  "attendance_details": {
    "1": "P",
    "2": "P",
    "3": "L",
    "4": "A"
  }
}
```

GET /students/id/activities?year={YY}&month={MM}&day={DD}&per\_page={NN}&page={PP}
```json
{
    "data": [
        {
            "id": "1-2",
            "date": "ISO_DATE",
            "start_time": "ISO_DATE_TIME",
            "end_time": "ISO_DATE_TIME",
            "subject": "English",
            "teacher_name": "Someone",
            "plans": [
                {
                    "title": "Activity plan title goes here.",
                    "details": "Activity plan detail goes here.",
                },
                {
                    "title": "Activity plan title goes here.",
                    "details": "Activity plan detail goes here.",
                }
            ],
            "activity_performance": {
                "rating": "[0 - 5]",
                "comments": "<html>Rich Text <b>Comments</b></html>"
            },
            "attachments": [
                {
                    "type": "IMAGE",
                    "url": {
                        "main": "http://...",
                        "thumb": "http://..."
                    }
                },
                {
                    "type": "IMAGE",
                    "url": {
                        "main": "http://...",
                        "thumb": "http://..."
                    }
                },
            ]
        },
        {
            "id": "2-3",
            "date": "2018-02-01",
            "start_time": "2018-02-01T12:00:00+05:45",
            "end_time": "2018-02-01T13:00:00+05:45",
            "subject": "Math",
            "teacher_name": "Someone",
            "plans": [],
            "activity_performance": null,
            "attachments": []
        }
    ],
    "meta": {
        "total": 35,
        "per_page": 20,
        "page": 1
    }
}
```

GET /students/id/leave_requests?year={y}&month={m}
```json
{
  "data": [
    {
      "id": 123,
      "date": "ISO_DATE",
      "no_of_days": 2,
      "reason": "some long reason",
      "is_approved": false
    }
  ],
  "meta": {
    "total": 1,
    "page": 1,
    "per_page": 10,
  }
}
```

POST /students/id/leave_requests

Request:
```json
{
  "date": "ISO_DATE",
  "no_of_days": 1,
  "reason": "some reason"
}
```
Response: Success
```json
{
  "id": 123,
  "date": "ISO_DATE",
  "no_of_days": 1,
  "reason": "some reason",
  "is_approved": false
}
```

PUT /students/id/leave_requests/id
```json
{
  "date": "ISO_DATE",
  "no_of_days": 1,
  "reason": "some reason"
}
```
Response: Success
```json
{
  "id": 123,
  "date": "ISO_DATE",
  "no_of_days": 1,
  "reason": "some reason",
  "is_approved": false
}
```

POST /students/id/leave_requests/id/cancel

Response: Success
```json
{
    "code": 200,
    "message": "Leave request deleted successfully"
}
```

GET /calendar/sync?last\_revision={XX}&per_page={YY}
```json
{
  "data": [
    {
      "id": 1,
      "date": "2017-01-01T00:00:00.000Z",
      "is_holiday": true,
      "title": "Some Event Holiday",
      "description": "blah blah ...... long text",
      "revision_no": 3
    }
  ],
  "meta": {
    "per_page": 10,
    "max_revision": 151,
    "last_revision": 5,
    "next_revision": 15,
  }
}
```

GET /notices/sync?last\_revision={XX}&per_page={YY}
```json
{
  "data": [
    {
      "id": 1,
      "type": "GENERAL",
      "title": "Some Notice",
      "description": "blah blah ...... long text",
      "revision_no": 3
    }
  ],
  "meta": {
    "per_page": 10,
    "max_revision": 151,
    "last_revision": 5,
    "next_revision": 15,
  }
}
```
