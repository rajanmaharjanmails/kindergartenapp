/**
 * Created by rajanmaharjan on 7/29/17.
 */

import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  Platform,
  TouchableOpacity,
} from 'react-native';

import {
  MyStatusBar,
} from '../components/1common/';

import cssV from '../styles/variables/cssV';
import session from '../utils/UserSession';
this.session = new session.Session();

import {
  NAV_START_SCREEN,
} from '../actions/types';

import { DrawerItems } from 'react-navigation';

export const CustomDrawerContentComponent = (props) => (
  <View style={styles.drawerContainerStyle}>
    <MyStatusBar
      backgroundColor={'rgba(1, 79, 112, 0.9)'}
      barStyle="light-content"
    />
    <View style={styles.headerContainerStyle}>
      <Image
        source={require('../assets/logo/Kindergarten-logo-name.png')}
        style={styles.headerImageStyle}
      />
    </View>
    <ScrollView style={styles.scrollViewContainer}>
      <DrawerItems {...props} style={styles.drawerItemsStyles} />
      <TouchableOpacity
        onPress={() => {
          this.session.logOutUser();
          props.navigation.dispatch({
            type: NAV_START_SCREEN,
          });
        }}
      >
        <Text style={{
          color: cssV('PRIMARY_WHITE'),
          fontSize: 16,
          fontWeight: '300',
          paddingTop: 10,
          paddingBottom: 10,
          marginLeft: 60,
          fontFamily: 'Sniglet-Regular',
        }}>
          Logout
        </Text>
      </TouchableOpacity>
    </ScrollView>
  </View>
);

const styles = StyleSheet.create({
  drawerContainerStyle: {
    flex: 1,
    backgroundColor: cssV('LIGHT_TEAL'),
  },
  headerContainerStyle: {
    height: 150,
    width: null,
    alignItems: 'center',
  },
  headerImageStyle: {
    flex: 1,
    alignItems: 'center',
    height: null,
    width: 130,
  },
  scrollViewContainer: {
    backgroundColor: 'transparent',
  },
  drawerItemsStyles: {
    // height: 500,
    marginTop: 0,
  },
});