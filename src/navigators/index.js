/**
 * Created by rajanmaharjan on 7/29/17.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BackHandler, Alert } from 'react-native';
import { connect } from 'react-redux';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';
import { AppNavigator } from './MainNavigator';
import RNExitApp from 'react-native-exit-app';

class AppWithNavigationState extends Component {
  constructor(props) {
    super();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props;
    if (nav.index === 0 && nav.routes[0].routeName === 'StartScreen') {
      Alert.alert('Exit Application?', 'Are you sure you want to exit?', [
        {
          text: 'Cancel',
          onPress: () => true,
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => RNExitApp.exitApp(),
        }]);
    }
    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    const { dispatch, nav } = this.props;
    return (
      <AppNavigator
        navigation={addNavigationHelpers({ dispatch, state: nav })}
      />
    );
  }
}

AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    nav: state.nav,
  };
};

export default connect(mapStateToProps)(AppWithNavigationState);