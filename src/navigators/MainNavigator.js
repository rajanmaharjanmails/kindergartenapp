/**
 * Created by rajanmaharjan on 8/19/17.
 */

import { StackNavigator, DrawerNavigator } from 'react-navigation';

import { CustomDrawerContentComponent } from './CustomDrawerContent';

import StartScreen from '../components/2StartScreen/StartScreen';
import CreateAccountScreen from '../components/3CreateAccScreen/CreateAccScreen';
import ProfileSetupScreen from '../components/4ProfileSetupScreen/ProfileSetupScreen';
import SignInScreen from '../components/5SignInScreen/SignInScreen';
import ManageStdScreen from '../components/6ManageStudentScreen/ManageStudentScreen';
import AddStdScreen from '../components/7AddStudentScreen/AddStudentScreen';
import ConfirmStdScreen from '../components/8ConfirmStudentScreen/ConfirmStudentScreen';
import ActivitiesDetailScreen from '../components/9ActivitiesScreen/ActivitiesDetailScreen';
import ActivitiesScreen from '../components/9ActivitiesScreen/ActivitiesScreen';
import PhotoAlbumScreen from '../components/10PhotoAlbumScreen/PhotoAlbumScreen';
import PhotoGalleryScreen from '../components/10PhotoAlbumScreen/PhotoGalleryScreen';
import ActivitiesCalendarScreen from '../components/11ActivitiesCalendarScreen/ActivitiesCalenderScreen';
import AttendanceReportScreen from '../components/12AttendanceReportScreen/AttendanceReportScreen';
import RequestLeaveScreen from '../components/13RequestLeaveScreen/RequestLeaveScreen';
import NoticesScreen from '../components/14NoticesScreen/NoticesScreen';
import ChatListScreen from '../components/15ChatScreen/ChatListScreen';
import ChatScreen from '../components/15ChatScreen/ChatScreen';

import cssV from '../styles/variables/cssV';

// console.warn(this.session.isLoggedIn().then((res) => {return true;}));
const SignInScreenStack = StackNavigator({
  SignInScreen: { screen: SignInScreen }
});

const CreateAccountScreenStack = StackNavigator({
  CreateAccountScreen: { screen: CreateAccountScreen }
});

const ProfileSetupScreenStack = StackNavigator({
  ProfileSetupScreen: { screen: ProfileSetupScreen }
});

const ActivitiesScreenStack = StackNavigator({
  ActivitiesStack: { screen: ActivitiesScreen },
  ActivitiesDetail: { screen: ActivitiesDetailScreen }
}, {
    headerMode: 'float',
    initialRouteName: 'ActivitiesStack'
  });

const PhotoAlbumScreenStack = StackNavigator({
  PhotoAlbumStack: { screen: PhotoAlbumScreen },
  PhotoGallery: { screen: PhotoGalleryScreen }
}, {
    headerMode: 'float',
    initialRouteName: 'PhotoAlbumStack'
  });

const ManageStdScreenStack = StackNavigator({
  ManageStdStack: { screen: ManageStdScreen },
  AddStdScreen: { screen: AddStdScreen },
  ConfirmStdScreen: { screen: ConfirmStdScreen }
}, {
    headerMode: 'float',
    initialRouteName: 'ManageStdStack'
  });

const ActivitiesCalendarScreenStack = StackNavigator({
  ActivitiesCalendarScreen: { screen: ActivitiesCalendarScreen }
});

const AttendanceReportScreenStack = StackNavigator({
  AttendanceReportScreen: { screen: AttendanceReportScreen }
});

const RequestLeaveScreenStack = StackNavigator({
  RequestLeaveScreen: { screen: RequestLeaveScreen }
});

const NoticesScreenStack = StackNavigator({
  NoticesScreen: { screen: NoticesScreen }
});

const ChatScreenStack = StackNavigator({
  ChatListScreen: { screen: ChatListScreen },
  ChatScreen: { screen: ChatScreen }
}, {
    headerMode: 'float',
    initialRouteName: 'ChatListScreen'
  });

const AppMainStack = DrawerNavigator({
  ManageStudents: { screen: ManageStdScreenStack },
  Activities: { screen: ActivitiesScreenStack },
  ActivitiesCalendar: { screen: ActivitiesCalendarScreenStack },
  NoticesScreen: { screen: NoticesScreenStack },
  AttendanceReport: { screen: AttendanceReportScreenStack },
  // Chats: {screen: ChatScreenStack},
  // PhotoAlbums: {screen: PhotoAlbumScreenStack},
  RequestLeave: { screen: RequestLeaveScreenStack }
}, {
    drawerPosition: 'right',
    initialRouteName: 'Activities',
    headerMode: 'float',
    contentComponent: CustomDrawerContentComponent,
    drawerWidth: 260,
    contentOptions: {
      activeTintColor: cssV('PRIMARY_WHITE'),
      activeBackgroundColor: cssV('DARK_TEAL'),
      inactiveTintColor: cssV('PRIMARY_WHITE'),
      labelStyle: {
        marginLeft: 0,
        marginTop: 5,
        marginBottom: 5,
        fontSize: 15,
        fontWeight: '400',
        padding: 5
      }
    }
  });

export const AppNavigator = StackNavigator({
  StartScreen: { screen: StartScreen },
  CreateAccountScreen: { screen: CreateAccountScreenStack },
  ProfileSetupScreen: { screen: ProfileSetupScreenStack },
  SignInScreen: { screen: SignInScreenStack },
  AppMainStack: { screen: AppMainStack }
}, {
    headerMode: 'none',
    initialRouteName: 'StartScreen'
  });
