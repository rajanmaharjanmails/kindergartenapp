/**
 * Created by rajanmaharjan on 9/17/17.
 */

import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../../navigators/MainNavigator';
import session from '../../utils/UserSession';

import {
  USER_PROFILE_NAME_CHANGED,
  USER_PROFILE_NUM_CHANGED,
  USER_PROFILE_ADDRESS_CHANGED,
  USER_PROFILE_UPDATE,
  USER_PROFILE_UPDATE_SUCCESS,
  USER_PROFILE_UPDATE_FAILED,
} from '../types';

const userProfileUpdateSuccess = (dispatch) => {
  dispatch({
    type: USER_PROFILE_UPDATE_SUCCESS,
  });

  AppNavigator.router.getStateForAction(
    dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'AppMainStack' }),
      ],
      key: null,
    })));
};

const userProfileUpdateFailed = (dispatch) => {
  dispatch({
    type: USER_PROFILE_UPDATE_FAILED,
  });
};

const profileNameChanged = (text) => {
  return {
    type: USER_PROFILE_NAME_CHANGED,
    payload: text,
  };
};

const profileNumChanged = (text) => {
  return {
    type: USER_PROFILE_NUM_CHANGED,
    payload: text,
  };
};

const profileAddressChanged = (text) => {
  return {
    type: USER_PROFILE_ADDRESS_CHANGED,
    payload: text,
  };
};

const updateUserProfile = ({ user_name, user_phone, user_address }) => {
  let data = {
    name: user_name,
    contact_no: user_phone,
    address: user_address,
  };

  return (dispatch) => {
    dispatch({
      type: USER_PROFILE_UPDATE,
    });

    if (!user_name || !user_phone || !user_address)
      return null;

    this.session = new session.Session();
    this.session.fetchSecure('put', '/me', data).then((response) => {
      const { problem, ok, data } = response;
      // console.warn(JSON.stringify(response));
      if (ok && data) {
        userProfileUpdateSuccess(dispatch);
      }
      if (problem) {
        // console.log(response);
        userProfileUpdateFailed(dispatch);
      }
    });
  };
};

module.exports = {
  profileNameChanged,
  profileNumChanged,
  profileAddressChanged,
  updateUserProfile,
};