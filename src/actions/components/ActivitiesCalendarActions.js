/**
 * Created by rajanmaharjan on 9/18/17.
 */
import session from '../../utils/UserSession';

import {
  CALENDAR_ACTIVITIES_FETCHING,
  CALENDAR_ACTIVITIES_FETCH_SUCCESS,
  CALENDAR_ACTIVITIES_FETCH_FAILED
} from '../types';

const getCalActivitiesSuccess = (dispatch, data) => {
  dispatch({
    type: CALENDAR_ACTIVITIES_FETCH_SUCCESS,
    payload: data
  }
  );
};

const getCalActivitiesFailed = (dispatch) => {
  dispatch({
    type: CALENDAR_ACTIVITIES_FETCH_FAILED
  });
};

const getCalActivities = () => {
  return (dispatch) => {
    dispatch({
      type: CALENDAR_ACTIVITIES_FETCHING
    });

    this.session = new session.Session();
    this.session.fetchSecure('get',
      `/calendar/sync`, null, {
        last_revision: 2,
        limit: 10
      }).then((response) => {
        // console.warn(JSON.stringify(response));
        const { problem, ok, data } = response;
        if (ok && data) {
          getCalActivitiesSuccess(dispatch, data);
        }
        if (problem) {
          // console.log(response);
          getCalActivitiesFailed(dispatch);
        }
      });
  };
};

module.exports = {
  getCalActivities
};
