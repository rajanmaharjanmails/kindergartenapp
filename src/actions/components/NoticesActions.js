/**
 * Created by rajanmaharjan on 9/22/17.
 */
import session from '../../utils/UserSession';

import {
  NOTICES_FETCHING,
  NOTICES_FETCH_SUCCESS,
  NOTICES_FETCH_FAILED,
  NOTICES_FILTER_CHANGED
} from '../types';

const getNoticesSuccess = (dispatch, data) => {
  dispatch({
    type: NOTICES_FETCH_SUCCESS,
    payload: data
  });
};

const getNoticesFailed = (dispatch) => {
  dispatch({
    type: NOTICES_FETCH_FAILED
  });
};

const noticesFilterChanged = (dispatch) => {
  dispatch({
    type: NOTICES_FILTER_CHANGED
  });
};

const getNotices = () => {
  return (dispatch) => {
    dispatch({
      type: NOTICES_FETCHING
    });

    this.session = new session.Session();
    this.session.fetchSecure('get',
      `/notices/sync`, null, {
        last_revision: 0,
        limit: 10
      }).then((response) => {
        // console.warn(JSON.stringify(response));
        const { problem, ok, data } = response;
        if (ok && data) {
          getNoticesSuccess(dispatch, data);
        }
        if (problem) {
          // console.log(response);
          getNoticesFailed(dispatch);
        }
      });
  };
};

module.exports = {
  getNotices,
  noticesFilterChanged
};
