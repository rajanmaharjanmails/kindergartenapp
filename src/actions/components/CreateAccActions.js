/**
 * Created by rajanmaharjan on 9/15/17.
 */

import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../../navigators/MainNavigator';
import session from '../../utils/UserSession';

import {
  REG_STUDENT_CODE_CHANGED,
  REG_EMAIL_CHANGED,
  REG_PASSWORD_CHANGED,
  REG_REPASSWORD_CHANGED,
  CREATE_USER_ACC,
  CREATE_USER_ACC_SUCCESS,
  CREATE_USER_ACC_FAILED
} from '../types';

const createUserAccSuccess = (dispatch) => {
  dispatch({
    type: CREATE_USER_ACC_SUCCESS
  });

  AppNavigator.router.getStateForAction(
    dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'ProfileSetupScreen' })
      ],
      key: null
    })));
};

const createUserAccFailed = (dispatch) => {
  dispatch({
    type: CREATE_USER_ACC_FAILED
  });
};

const regStudentCodeChanged = (text) => {
  return {
    type: REG_STUDENT_CODE_CHANGED,
    payload: text
  };
};

const regEmailChanged = (text) => {
  return {
    type: REG_EMAIL_CHANGED,
    payload: text
  };
};

const regPasswordChanged = (text) => {
  return {
    type: REG_PASSWORD_CHANGED,
    payload: text
  };
};

const regRePasswordChanged = (text) => {
  return {
    type: REG_REPASSWORD_CHANGED,
    payload: text
  };
};

const createUserAcc = ({ reg_code: regCode, reg_email: regEmail, reg_password: regPassword, reg_re_password: regRePassword }) => {
  return (dispatch) => {
    dispatch({
      type: CREATE_USER_ACC
    });

    if (!regPassword || (regPassword !== regRePassword)) { return null; }

    this.session = new session.Session();
    this.session.register(regCode, regEmail, regRePassword)
      .then((response) => {
        const { problem, ok, data } = response;

        if (ok && data && data.access_token) {
          createUserAccSuccess(dispatch);
        }

        if (problem) {
          // console.log(response);
          createUserAccFailed(dispatch);
        }
      });
  };
};

module.exports = {
  regStudentCodeChanged,
  regEmailChanged,
  regPasswordChanged,
  regRePasswordChanged,
  createUserAcc
};
