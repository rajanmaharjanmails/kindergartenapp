/**
 * Created by rajanmaharjan on 9/21/17.
 */
import session from '../../utils/UserSession';

import {
  ATTENDANCE_REPORT_FETCHING,
  ATTENDANCE_REPORT_FETCH_SUCCESS,
  ATTENDANCE_REPORT_FETCH_FAILED,
} from '../types';

const getAttendanceReportSuccess = (dispatch, data) => {
  dispatch({
    type: ATTENDANCE_REPORT_FETCH_SUCCESS,
    payload: data,
  },
  );
};

const getAttendanceReportFailed = (dispatch) => {
  dispatch({
    type: ATTENDANCE_REPORT_FETCH_FAILED,
  });
};

const getAttendanceReport = (year, month) => {
  return (dispatch) => {
    dispatch({
      type: ATTENDANCE_REPORT_FETCHING,
    });

    this.session = new session.Session();
    this.session.getStudentId().then((studentId) => {
      if (studentId) {
        let id = JSON.parse(studentId);
        this.session.fetchSecure('get',
          `/students/${id}/attendance_report`, null, {
            year: year,
            month: month,
          }).then((response) => {
            // console.warn(JSON.stringify(response));
            const { problem, ok, data } = response;
            if (ok && data) {
              getAttendanceReportSuccess(dispatch, data);
            }
            if (problem) {
              // console.log(response);
              getAttendanceReportFailed(dispatch);
            }
          });
      } else {
        getAttendanceReportFailed(dispatch);
      }
    });
  };
};

module.exports = {
  getAttendanceReport,
};