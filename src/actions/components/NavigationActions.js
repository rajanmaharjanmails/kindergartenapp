/**
 * Created by rajanmaharjan on 8/28/17.
 */

import {
  NAV_START_SCREEN,
  NAV_CREATE_ACC_SCREEN,
  NAV_SINGIN_SCREEN,
  NAV_BACK
} from '../types';

export const navigateStartScreen = ({ prop, value }) => {
  return {
    type: NAV_START_SCREEN,
    payload: { prop, value }
  };
};

export const navigateCreateAccScreen = ({ prop, value }) => {
  return {
    type: NAV_CREATE_ACC_SCREEN,
    payload: { prop, value }
  };
};

export const navigateSingInScreen = ({ prop, value }) => {
  return {
    type: NAV_SINGIN_SCREEN,
    payload: { prop, value }
  };
};

export const navigateBack = ({ prop, value }) => {
  return {
    type: NAV_BACK,
    payload: { prop, value }
  };
};
