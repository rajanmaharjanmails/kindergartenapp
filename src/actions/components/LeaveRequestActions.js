/**
 * Created by rajanmaharjan on 9/22/17.
 */
import session from '../../utils/UserSession';

import {
  LEAVE_REQUEST_FETCHING,
  LEAVE_REQUEST_FETCH_SUCCESS,
  LEAVE_REQUEST_FETCH_FAILED,
  LEAVE_REQUEST_DATE_CHANGED,
  LEAVE_REQUEST_DAYS_NO_CHANGED,
  LEAVE_REQUEST_REASON_CHANGED,
  LEAVE_REQUEST_CONFIRM,
  LEAVE_REQUEST_CONFIRM_SUCCESS,
  LEAVE_REQUEST_CONFIRM_FAILED,
} from '../types';

const getLeaveRequestsSuccess = (dispatch, data) => {
  dispatch({
    type: LEAVE_REQUEST_FETCH_SUCCESS,
    payload: data
  });
};

const leaveRequestConfirmSuccess = (dispatch) => {
  dispatch({
    type: LEAVE_REQUEST_CONFIRM_SUCCESS,
  });
};

const getLeaveRequestsFailed = (dispatch) => {
  dispatch({
    type: LEAVE_REQUEST_FETCH_FAILED,
  });
};

const leaveRequestConfirmFailed = (dispatch) => {
  dispatch({
    type: LEAVE_REQUEST_CONFIRM_FAILED,
  });
};

const leaveRequestDateChanged = (text) => {
  return {
    type: LEAVE_REQUEST_DATE_CHANGED,
    payload: text,
  };
};

const leaveRequestDaysNoChanged = (text) => {
  return {
    type: LEAVE_REQUEST_DAYS_NO_CHANGED,
    payload: text,
  };
};

const leaveRequestReasonChanged = (text) => {
  return {
    type: LEAVE_REQUEST_REASON_CHANGED,
    payload: text,
  };
};

const getLeaveRequests = (year, month) => {
  return (dispatch) => {
    dispatch({
      type: LEAVE_REQUEST_FETCHING,
    });

    this.session = new session.Session();
    this.session.getStudentId().then((studentId) => {
      if (studentId) {
        let id = JSON.parse(studentId);
        this.session.fetchSecure('get',
          `/students/${id}/leave_requests`, null, {
            year: year,
            month: month,
            per_page: 32,
          }).then((response) => {
            // console.warn(JSON.stringify(response));
            const { problem, ok, data } = response;
            if (ok && data) {
              getLeaveRequestsSuccess(dispatch, data);
            }
            if (problem) {
              // console.log(response);
              getLeaveRequestsFailed(dispatch);
            }
          });
      } else {
        getLeaveRequestsFailed(dispatch);
      }
    });
  };
};

const leaveRequestsConfirm = (requestLeaveDate, no_of_days,
  reason) => {
  let leaveRequestDetails = {
    date: requestLeaveDate, no_of_days, reason,
  };
  return (dispatch) => {
    dispatch({
      type: LEAVE_REQUEST_CONFIRM,
    });

    if (!requestLeaveDate || !no_of_days || !reason)
      return null;

    this.session = new session.Session();
    this.session.getStudentId().then((studentId) => {
      if (studentId) {
        let id = JSON.parse(studentId);
        this.session.fetchSecure('post',
          `/students/${id}/leave_requests`, leaveRequestDetails).
          then((response) => {
            const { problem, ok, data } = response;
            if (ok && data) {
              leaveRequestConfirmSuccess(dispatch, data);
            }
            if (problem) {
              leaveRequestConfirmFailed(dispatch);
            }
          });
      } else {
        leaveRequestConfirmFailed(dispatch);
      }
    });
  };
};

module.exports = {
  getLeaveRequests,
  leaveRequestsConfirm,
  leaveRequestDateChanged,
  leaveRequestDaysNoChanged,
  leaveRequestReasonChanged,
};