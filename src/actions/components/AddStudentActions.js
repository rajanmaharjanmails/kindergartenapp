/**
 * Created by rajanmaharjan on 9/18/17.
 */
/* eslint-disable camelcase */
import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../../navigators/MainNavigator';
import session from '../../utils/UserSession';

import {
  ADD_STUDENT_CODE_CHANGED,
  STUDENT_CODE_USE,
  STUDENT_CODE_USE_SUCCESS,
  STUDENT_CODE_USE_FAILED,
  STUDENT_CODE_USE_CONFIRM,
  STUDENT_CODE_USE_CONFIRM_SUCCESS,
  STUDENT_CODE_USE_CONFIRM_FAILED,
  STUDENT_CODE_USE_CANCEL
} from '../types';

const useStudentCodeSuccess = (dispatch, studentCode, data) => {
  dispatch({
    type: STUDENT_CODE_USE_SUCCESS,
    payload: { studentCode, data }
  });

  AppNavigator.router.getStateForAction(
    dispatch(NavigationActions.navigate({ routeName: 'ConfirmStdScreen' }))
  );
};

const useStudentCodeConfirmSuccess = (dispatch) => {
  dispatch({
    type: STUDENT_CODE_USE_CONFIRM_SUCCESS
  });

  AppNavigator.router.getStateForAction(
    dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'ManageStdStack' })
      ],
      key: 'ManageStdStack'
    })));
};

const useStudentCodeConfirmFailed = (dispatch) => {
  dispatch({
    type: STUDENT_CODE_USE_CONFIRM_FAILED
  });
};

const useStudentCodeCancel = (studentCode) => {
  return (dispatch) => {
    dispatch({
      type: STUDENT_CODE_USE_CANCEL,
      payload: studentCode
    });

    AppNavigator.router.getStateForAction(
      dispatch(NavigationActions.back())
    );
  };
};

const useStudentCodeFailed = (dispatch) => {
  dispatch({
    type: STUDENT_CODE_USE_FAILED
  });
};

const studentCodeChanged = (text) => {
  return {
    type: ADD_STUDENT_CODE_CHANGED,
    payload: text
  };
};

const useStudentCode = ({ student_code }) => {
  return (dispatch) => {
    dispatch({
      type: STUDENT_CODE_USE
    });

    if (!student_code) { return null; }

    this.session = new session.Session();
    this.session.fetchSecure('get', `/student_info/${student_code}`)
      .then((response) => {
        const { problem, ok, data } = response;
        // console.warn(JSON.stringify(response));
        if (ok && data) {
          useStudentCodeSuccess(dispatch, student_code, data);
        }
        if (problem) {
          // console.log(response);
          useStudentCodeFailed(dispatch);
        }
      });
  };
};

const confirmStudentCode = ({ student_code }) => {
  return (dispatch) => {
    dispatch({
      type: STUDENT_CODE_USE_CONFIRM
    });

    if (!student_code) { return null; }

    this.session = new session.Session();
    this.session.fetchSecure('post', '/me/students/subscribe', {
      code: student_code
    }).then((response) => {
      const { problem, ok, data } = response;
      // console.warn(JSON.stringify(response));
      if (ok && data) {
        useStudentCodeConfirmSuccess(dispatch);
      }
      if (problem) {
        // console.log(response);
        useStudentCodeConfirmFailed(dispatch);
      }
    });
  };
};

module.exports = {
  studentCodeChanged,
  useStudentCode,
  confirmStudentCode,
  useStudentCodeCancel
};
