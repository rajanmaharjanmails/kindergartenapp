/**
 * Created by rajanmaharjan on 9/11/17.
 */
import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../../navigators/MainNavigator';
import session from '../../utils/UserSession';

import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  LOGIN_USER,
} from '../types';

const loginUserSuccess = (dispatch) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
  });

  AppNavigator.router.getStateForAction(
    dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'AppMainStack' }),
      ],
      key: null,
    })));
};

const loginUserFailed = (dispatch) => {
  dispatch({
    type: LOGIN_USER_FAILED,
  });
};

const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text,
  };
};

const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text,
  };
};

const loginUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({
      type: LOGIN_USER,
    });

    loginUserSuccess(dispatch);

    // this.session = new session.Session();
    // this.session.login(email, password).
    //   then((response) => {
    //     const { problem, ok, data } = response;

    //     if (ok && data && data.access_token) {
    //       loginUserSuccess(dispatch);
    //     }

    //     if (problem) {
    //       // console.log(response);
    //       loginUserFailed(dispatch);
    //     }
    //   });
  };
};

module.exports = {
  emailChanged,
  passwordChanged,
  loginUser,
};