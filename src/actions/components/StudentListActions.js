/**
 * Created by rajanmaharjan on 9/17/17.
 */
import session from '../../utils/UserSession';

import {
  STUDENTS_LIST_FETCHING,
  STUDENTS_FETCH_SUCCESS,
  STUDENTS_FETCH_FAILED,
  STUDENT_UNSUBSCRIBE,
  STUDENT_UNSUBSCRIBE_FAILED,
  STUDENT_UNSUBSCRIBE_SUCCESS
} from '../types';

this.session = new session.Session();

const getStudentListSuccess = (dispatch, data) => {
  dispatch({
    type: STUDENTS_FETCH_SUCCESS,
    payload: {
      data: [{
        name: "Rajan Maharjan",
        class: "UKG"
      }]
    },
  },
  );
};

const getStudentListFailed = (dispatch) => {
  dispatch({
    type: STUDENTS_FETCH_FAILED,
  });
};

const getStudentList = () => {
  return (dispatch) => {
    dispatch({
      type: STUDENTS_LIST_FETCHING,
    });

    getStudentListSuccess(dispatch);

    // this.session.fetchSecure('get', '/me/students').then((response) => {
    //   const { problem, ok, data } = response;
    //   // console.warn(JSON.stringify(response));
    //   if (ok && data) {
    //     getStudentListSuccess(dispatch, data);
    //   }
    //   if (problem) {
    //     // console.log(response);
    //     getStudentListFailed(dispatch);
    //   }
    // });
  };
};

const selectStudent = (studentId) => {
  return () => {
    this.session.saveStudentId(studentId);
  };
};

const unsubscribeStudentSuccess = (dispatch, data) => {
  dispatch({
    type: STUDENT_UNSUBSCRIBE_SUCCESS,
    payload: data,
  },
  );
};

const unsubscribeStudentFailed = (dispatch) => {
  dispatch({
    type: STUDENT_UNSUBSCRIBE_FAILED,
  });
};

const unsubscribeStudent = (studentId) => {

  return (dispatch) => {
    dispatch({
      type: STUDENT_UNSUBSCRIBE,
    });

    this.session.fetchSecure('post', `/me/students/${studentId}/unsubscribe`).then((response) => {
      const { problem, ok, data } = response;
      // console.warn(JSON.stringify(response));
      if (ok && data) {
        unsubscribeStudentSuccess(dispatch, data);
      }
      if (problem) {
        // console.log(response);
        unsubscribeStudentFailed(dispatch);
      }
    });
  };
};

module.exports = {
  getStudentList,
  selectStudent,
  unsubscribeStudent
};