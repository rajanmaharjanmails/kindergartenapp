/**
 * Created by rajanmaharjan on 7/29/17.
 */

/*
 * NAV ACTION TYPES
 */
export const NAV_START_SCREEN = 'nav_start_screen';
export const NAV_CREATE_ACC_SCREEN = 'nav_create_acc_screen';
export const NAV_PROFILE_SETUP_SCREEN = 'nav_profile_setup_screen';
export const NAV_SINGIN_SCREEN = 'nav_singin_screen';
export const NAV_MANAGE_STD_SCREEN = 'nav_manage_std_screen';
export const NAV_ADD_STD_SCREEN = 'nav_add_std_screen';
export const NAV_CONFIRM_STD_SCREEN = 'nav_confirm_std_screen';
export const NAV_ACTIVITIES_SCREEN = 'nav_activities_screen';
export const NAV_ACTIVITIES_DETAIL_SCREEN = 'nav_activities_detail_screen';
export const NAV_PHOTO_ALMBUMS_SCREEN = 'nav_photo_albums_screen';
export const NAV_PHOTO_GALLERY_SCREEN = 'nav_photo_gallery_screen';
export const NAV_ACT_CALENDAR_SCREEN = 'nav_act_calendar_screen';
export const NAV_ATT_REPORT_SCREEN = 'nav_att_report_screen';
export const NAV_REQ_LEAVE_SCREEN = 'nav_req_leave_screen';
export const NAV_NOTICES_SCREEN = 'nav_notices_screen';
export const NAV_CHAT_LIST_SCREEN = 'nav_chat_list_screen';
export const NAV_CHAT_SCREEN = 'nav_chat_screen';
export const NAV_BACK = 'nav_back';

/*
 * AUTH ACTION TYPES
 */
export const EMAIL_CHANGED = 'login_email_changed';
export const PASSWORD_CHANGED = 'login_password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAILED = 'login_user_failed';
export const LOGIN_USER = 'login_user';

/*
 * CREATE ACCOUNT ACTION TYPES
 */
export const REG_STUDENT_CODE_CHANGED = 'reg_student_code_changed';
export const REG_EMAIL_CHANGED = 'reg_email_changed';
export const REG_PASSWORD_CHANGED = 'reg_pass_success';
export const REG_REPASSWORD_CHANGED = 'reg_repass_success';
export const CREATE_USER_ACC = 'create_user_acc';
export const CREATE_USER_ACC_SUCCESS = 'create_user_acc_success';
export const CREATE_USER_ACC_FAILED = 'create_user_acc_failed';

/*
 * USER PROFILE UPDATE ACTION TYPES
 */
export const USER_PROFILE_NAME_CHANGED = 'user_profile_name_changed';
export const USER_PROFILE_NUM_CHANGED = 'user_profile_num_changed';
export const USER_PROFILE_ADDRESS_CHANGED = 'user_profile_address_changed';
export const USER_PROFILE_UPDATE = 'user_profile_update';
export const USER_PROFILE_UPDATE_SUCCESS = 'user_profile_update_success';
export const USER_PROFILE_UPDATE_FAILED = 'user_profile_update_failed';

/*
 * STUDENTS LIST ACTION TYPES
 */
export const STUDENTS_LIST_FETCHING = 'students_list_fetching';
export const STUDENTS_FETCH_SUCCESS = 'students_fetch_success';
export const STUDENTS_FETCH_FAILED = 'students_fetch_failed';
export const STUDENT_UNSUBSCRIBE = 'student_unsubscribe';
export const STUDENT_UNSUBSCRIBE_FAILED = 'student_unsubscribe_failed';
export const STUDENT_UNSUBSCRIBE_SUCCESS = 'student_unsubscribe_success';

/*
 * ADD STUDENT ACTION TYPES
 */
export const ADD_STUDENT_CODE_CHANGED = 'add_student_code_changed';
export const STUDENT_CODE_USE = 'student_code_use';
export const STUDENT_CODE_USE_SUCCESS = 'student_code_use_success';
export const STUDENT_CODE_USE_FAILED = 'student_code_use_failed';
export const STUDENT_CODE_USE_CONFIRM = 'student_code_use_confirm';
export const STUDENT_CODE_USE_CONFIRM_SUCCESS = 'student_code_use_confirm_success';
export const STUDENT_CODE_USE_CONFIRM_FAILED = 'student_code_use_confirm_failed';
export const STUDENT_CODE_USE_CANCEL = 'student_code_use_cancel';

/*
 * CALENDAR ACTIVITIES ACTION TYPES
 */
export const CALENDAR_ACTIVITIES_FETCHING = 'calendar_activities_fetching';
export const CALENDAR_ACTIVITIES_FETCH_SUCCESS = 'calendar_activities_fetch_success';
export const CALENDAR_ACTIVITIES_FETCH_FAILED = 'calendar_activities_fetch_failed';

/*
 * ATTENDANCE REPORT ACTION TYPES
 */
export const ATTENDANCE_REPORT_FETCHING = 'attendance_report_fetching';
export const ATTENDANCE_REPORT_FETCH_SUCCESS = 'attendance_report_fetch_success';
export const ATTENDANCE_REPORT_FETCH_FAILED = 'attendance_report_fetch_failed';

/*
 * LEAVE REQUEST ACTION TYPES
 */
export const LEAVE_REQUEST_FETCHING = 'leave_request_fetching';
export const LEAVE_REQUEST_FETCH_SUCCESS = 'leave_request_fetch_success';
export const LEAVE_REQUEST_FETCH_FAILED = 'leave_request_fetch_failed';
export const LEAVE_REQUEST_DATE_CHANGED = 'leave_request_date_changed';
export const LEAVE_REQUEST_DAYS_NO_CHANGED = 'leave_request_days_no_changed';
export const LEAVE_REQUEST_REASON_CHANGED = 'leave_request_reason_changed';
export const LEAVE_REQUEST_CONFIRM = 'leave_request_confirm';
export const LEAVE_REQUEST_CONFIRM_SUCCESS = 'leave_request_confirm_success';
export const LEAVE_REQUEST_CONFIRM_FAILED = 'leave_request_confirm_failed';

/*
 * NOTICES ACTION TYPES
 */
export const NOTICES_FETCHING = 'notices_fetching';
export const NOTICES_FETCH_SUCCESS = 'notices_fetch_success';
export const NOTICES_FETCH_FAILED = 'notices_fetch_failed';
export const NOTICES_FILTER_CHANGED = 'notices_filter_changed';