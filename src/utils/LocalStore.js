/**
 * Created by rajanmaharjan on 9/13/17.
 */
import { AsyncStorage } from 'react-native';

export default class LocalStore {
  async saveItem(item, selectedValue) {
    try {
      return await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  async multiSaveItem(items) {
    try {
      return await AsyncStorage.multiSet(items);
    } catch (error) {
      console.error('AsyncStorage error: ' + error);
    }
  }

  async getItem(item, selectedValue) {
    try {
      return await AsyncStorage.getItem(item, selectedValue);
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  async removeItem(item, selectedValue) {
    try {
      return await AsyncStorage.removeItem(item, selectedValue);
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  async clearAll() {
    try {
      return await AsyncStorage.clear();
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }
}
