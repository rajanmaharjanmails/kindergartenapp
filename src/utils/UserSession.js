/**
 * Created by rajanmaharjan on 9/12/17.
 */
import {
  create
} from 'apisauce';
import LocalStore from './LocalStore';
import moment from 'moment';
import jwtDecode from 'jwt-decode';

class Session {
  constructor() {
    this.localStore = new LocalStore();
    this.api = create({
      baseURL: 'http://nagbahal.com/kindergarten/api/v1'
    });
  }

  saveToken(data) {
    const parsedJWT = jwtDecode(data.access_token);
    // console.warn(JSON.stringify(parsedJWT));
    this.localStore.multiSaveItem([
      ['access_token', JSON.stringify(data.access_token)],
      ['refresh_token', JSON.stringify(data.refresh_token)],
      ['exp_time', JSON.stringify(moment(parsedJWT.exp * 1000))],
      ['user_id', JSON.stringify(parsedJWT.user_id)]
    ]);
  }

  saveStudentId(studentId) {
    this.localStore.saveItem('studentId', studentId.toString());
  }

  async getStudentId() {
    return await this.localStore.getItem('studentId').then((resp) => JSON.parse(resp));
  }

  register(code, email, password) {
    this.code = code;
    this.email = email;
    this.password = password;
    return this.api.post('/register', {
      code: this.code,
      email: this.email,
      password: this.password
    }).then((response) => {
      if (response.ok && response.data && response.data.access_token) {
        this.saveToken(response.data);
        this.fetchSecure('get', '/me/students').then((students) => {
          const {
            problem,
            ok,
            data
          } = students;
          if (ok && data && data.data.length) {
            this.saveStudentId(data.data[0].id);
          }
          if (problem) {
            return null;
          }
        });
      }
      return response;
    });
  }

  login(email, password) {
    this.email = email;
    this.password = password;

    return this.api.post('/authenticate', {
      email: this.email,
      password: this.password
    }).then((response) => {
      if (response.ok && response.data && response.data.access_token) {
        this.saveToken(response.data);
        this.fetchSecure('get', '/me/students').then((students) => {
          const {
            problem,
            ok,
            data
          } = students;
          if (ok && data && data.data.length) {
            this.saveStudentId(data.data[0].id);
          }
          if (problem) {
            return null;
          }
        });
      }
      return response;
    });
  }

  async refreshToken(refresh_token) {
    try {
      return await this.api.post('/authenticate/refresh', {
        refresh_token: refresh_token
      }).then((response) => {
        if (response.ok && response.data && response.data.access_token) {
          this.saveToken(response.data);
        }
        return this.localStore.getItem('access_token');
      });
    } catch (error) {
      return error;
    }
  }

  isLoggedIn() {
    return this.localStore.getItem('access_token').then((response) => {
      return !!response;
    });
  }

  logOutUser() {
    this.localStore.clearAll();
  }

  ensureLogin() {
    return this.localStore.getItem('exp_time').then((exp_time) => {
      if (exp_time && moment() >= moment(JSON.parse(exp_time))) {
        this.localStore.getItem('refresh_token').then((refresh_token) => {
          // console.warn('getting a new session', JSON.parse(refresh_token));
          return this.refreshToken(JSON.parse(refresh_token))
            .then((response) => response);
        });
      }
      return this.localStore.getItem('access_token');
    });
  }

  fetchSecure(method, url, data, params) {
    return this.ensureLogin().then((access_token) => {
      access_token = JSON.parse(access_token);
      if (access_token && url) {
        // this.api.setHeader('Authorization', access_token);
        // this.api.setHeaders({
        //   'Authorization': `bearer ${access_token}`,
        // });

        this.api.addRequestTransform(request => {
          request.headers = {
            'Authorization': `bearer ${access_token}`
          };
          if (params) {
            for (let key in params) {
              if (params.hasOwnProperty(key)) {
                request.params[key] = params[key];
              }
            }
          }
        });

        switch (method) {
          case 'get':
            return this.api.get(url).then((response) => response);
          case 'post':
            return this.api.post(url, data).then((response) => response);
          case 'put':
            return this.api.put(url, data).then((response) => response);
          case 'delete':
            return this.api.delete(url).then((response) => response);
          default:
            return this.api.get(url).then((response) => response);
        }
      } else {
        // throw new Error('User not logged in.');
        return {
          problem: 'CLIENT_ERROR',
          data: 'User not logged in.'
        };
      }
    });
  }
}

module.exports.Session = Session;
