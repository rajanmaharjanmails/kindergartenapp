/**
 * Created by rajanmaharjan on 9/18/17.
 */
import {
  CALENDAR_ACTIVITIES_FETCHING,
  CALENDAR_ACTIVITIES_FETCH_SUCCESS,
  CALENDAR_ACTIVITIES_FETCH_FAILED
} from '../../actions/types';

const INITIAL_STATE = {
  calActivitiesData: [],
  dataFetched: false,
  isFetching: false,
  error: false
};

export default (state = INITIAL_STATE, action) => {
  // console.log('CALENDAR ACTIVITIES', action);
  switch (action.type) {
    case CALENDAR_ACTIVITIES_FETCHING:
      return {
        ...state,
        isFetching: true
      };
    case CALENDAR_ACTIVITIES_FETCH_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        isFetching: false,
        calActivitiesData: action.payload.data,
        dataFetched: true,
        error: false
      };
    case CALENDAR_ACTIVITIES_FETCH_FAILED:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    default:
      return state;
  }
};
