/**
 * Created by rajanmaharjan on 9/16/17.
 */
import {
  REG_STUDENT_CODE_CHANGED,
  REG_EMAIL_CHANGED,
  REG_PASSWORD_CHANGED,
  REG_REPASSWORD_CHANGED,
  CREATE_USER_ACC,
  CREATE_USER_ACC_SUCCESS,
  CREATE_USER_ACC_FAILED
} from '../../actions/types';

const INITIAL_STATE = {
  reg_code: '',
  reg_email: '',
  reg_password: null,
  reg_re_password: null,
  reg_user: null,
  reg_error: '',
  reg_loading: false
};

export default (state = INITIAL_STATE, action) => {
  // console.log('Create Acc Action', action);
  switch (action.type) {
    case REG_STUDENT_CODE_CHANGED:
      return { ...state, reg_code: action.payload, reg_error: '' };
    case REG_EMAIL_CHANGED:
      return { ...state, reg_email: action.payload, reg_error: '' };
    case REG_PASSWORD_CHANGED:
      return { ...state, reg_password: action.payload, reg_error: '' };
    case REG_REPASSWORD_CHANGED:
      return {
        ...state,
        reg_re_password: action.payload,
        reg_error: ''
      };
    case CREATE_USER_ACC:
      if (!state.reg_code || !state.reg_email || !state.reg_password ||
        !state.reg_re_password) {
        return {
          ...state,
          reg_error: 'Please fill all required fields!',
          reg_loading: false
        };
      }

      if (state.reg_password !== state.reg_re_password) {
        return {
          ...state,
          reg_error: 'Password didn\'t match!',
          reg_password: '',
          reg_re_password: '',
          reg_loading: false
        };
      }

      return { ...state, reg_loading: true, reg_error: '' };
    case CREATE_USER_ACC_SUCCESS:
      return {
        ...state, ...INITIAL_STATE, reg_loading: true, reg_error: ''
      };
    case CREATE_USER_ACC_FAILED:
      return {
        ...state,
        reg_error: 'Please re-check your submitted information!',
        reg_password: '',
        re_password: '',
        reg_loading: false
      };
    default:
      return state;
  }
};
