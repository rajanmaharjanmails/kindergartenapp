/**
 * Created by rajanmaharjan on 9/21/17.
 */
import {
  ATTENDANCE_REPORT_FETCHING,
  ATTENDANCE_REPORT_FETCH_SUCCESS,
  ATTENDANCE_REPORT_FETCH_FAILED
} from '../../actions/types';

const INITIAL_STATE = {
  attendanceReportData: {},
  dataFetched: false,
  isFetching: false,
  error: false
};

export default (state = INITIAL_STATE, action) => {
  // console.warn('ATTENDANCE_REPORT', action);
  switch (action.type) {
    case ATTENDANCE_REPORT_FETCHING:
      return {
        ...state,
        isFetching: true
      };
    case ATTENDANCE_REPORT_FETCH_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        isFetching: false,
        attendanceReportData: action.payload,
        dataFetched: true,
        error: false
      };
    case ATTENDANCE_REPORT_FETCH_FAILED:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    default:
      return state;
  }
};
