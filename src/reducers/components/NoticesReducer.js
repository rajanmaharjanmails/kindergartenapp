/**
 * Created by rajanmaharjan on 9/22/17.
 */
import {
  NOTICES_FETCHING,
  NOTICES_FETCH_SUCCESS,
  NOTICES_FETCH_FAILED,
  NOTICES_FILTER_CHANGED
} from '../../actions/types';

const INITIAL_STATE = {
  noticesData: {},
  dataFetched: false,
  isFetching: false,
  error: false
};

export default (state = INITIAL_STATE, action) => {
  // console.warn('Notices Reducer', action);
  switch (action.type) {
    case NOTICES_FETCHING:
      return {
        ...state,
        isFetching: true
      };
    case NOTICES_FETCH_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        isFetching: false,
        noticesData: action.payload.data,
        dataFetched: true,
        error: false
      };
    case NOTICES_FETCH_FAILED:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    default:
      return state;
  }
};
