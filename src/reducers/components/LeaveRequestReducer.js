/**
 * Created by rajanmaharjan on 9/22/17.
 */
import {
  LEAVE_REQUEST_FETCHING,
  LEAVE_REQUEST_FETCH_SUCCESS,
  LEAVE_REQUEST_FETCH_FAILED,
  LEAVE_REQUEST_DATE_CHANGED,
  LEAVE_REQUEST_DAYS_NO_CHANGED,
  LEAVE_REQUEST_REASON_CHANGED,
  LEAVE_REQUEST_CONFIRM,
  LEAVE_REQUEST_CONFIRM_SUCCESS,
  LEAVE_REQUEST_CONFIRM_FAILED
} from '../../actions/types';

const INITIAL_STATE = {
  leaveRequestsData: {},
  requestLeaveDate: '',
  requestLeaveDaysNo: '',
  requestLeaveReason: '',
  dataFetched: false,
  isFetching: false,
  error: false,
  reqLeaveError: false,
  requestingLeave: false,
  reqLeaveMsg: null,
  requestSuccess: null
};

export default (state = INITIAL_STATE, action) => {
  // console.warn('Leave requests', action);
  switch (action.type) {
    case LEAVE_REQUEST_FETCHING:
      return {
        ...state,
        isFetching: true,
        reqLeaveError: false
      };
    case LEAVE_REQUEST_FETCH_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        isFetching: false,
        leaveRequestsData: action.payload.data,
        dataFetched: true,
        error: false
      };
    case LEAVE_REQUEST_FETCH_FAILED:
      return {
        ...state,
        isFetching: false,
        error: true,
        reqLeaveError: false
      };
    case LEAVE_REQUEST_DATE_CHANGED:
      return { ...state, requestLeaveDate: action.payload, reqLeaveError: false };
    case LEAVE_REQUEST_DAYS_NO_CHANGED:
      return {
        ...state,
        requestLeaveDaysNo: action.payload,
        reqLeaveError: false
      };
    case LEAVE_REQUEST_REASON_CHANGED:
      return {
        ...state,
        requestLeaveReason: action.payload,
        reqLeaveError: false
      };
    case LEAVE_REQUEST_CONFIRM:
      if (!state.requestLeaveDate || !state.requestLeaveDaysNo ||
        !state.requestLeaveReason) {
        return {
          ...state,
          requestingLeave: false,
          reqLeaveError: true,
          reqLeaveMsg: 'Please fill all required fields!'
        };
      }

      return { ...state, requestingLeave: true, reqLeaveError: false };
    case LEAVE_REQUEST_CONFIRM_SUCCESS:
      return {
        ...state,
        requestingLeave: false,
        reqLeaveError: false,
        requestLeaveDate: '',
        requestLeaveDaysNo: '',
        requestLeaveReason: '',
        reqLeaveMsg: null,
        requestSuccess: true
      };
    case LEAVE_REQUEST_CONFIRM_FAILED:
      return {
        ...state,
        requestingLeave: false,
        reqLeaveError: true,
        reqLeaveMsg: null,
        requestSuccess: false
      };
    default:
      return state;
  }
};
