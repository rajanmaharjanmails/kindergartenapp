/**
 * Created by rajanmaharjan on 7/29/17.
 */

import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../../navigators/MainNavigator';
// import { StatusBar } from 'react-native';

import {
  NAV_START_SCREEN,
  NAV_CREATE_ACC_SCREEN,
  NAV_PROFILE_SETUP_SCREEN,
  NAV_SINGIN_SCREEN,
  NAV_MANAGE_STD_SCREEN,
  NAV_ADD_STD_SCREEN,
  NAV_CONFIRM_STD_SCREEN,
  NAV_ACTIVITIES_SCREEN,
  NAV_ACTIVITIES_DETAIL_SCREEN,
  NAV_PHOTO_ALMBUMS_SCREEN,
  NAV_PHOTO_GALLERY_SCREEN,
  NAV_ACT_CALENDAR_SCREEN,
  NAV_ATT_REPORT_SCREEN,
  NAV_REQ_LEAVE_SCREEN,
  NAV_NOTICES_SCREEN,
  NAV_CHAT_LIST_SCREEN,
  NAV_CHAT_SCREEN,
  NAV_BACK
} from '../../actions/types';

const INITIAL_NAV_STATE = AppNavigator.router.getStateForAction(
  AppNavigator.router.getStateForAction(
    AppNavigator.router.getActionForPathAndParams(
      'StartScreen'
    )));

export default (state = INITIAL_NAV_STATE, action) => {
  let nextState;
  switch (action.type) {
    case NAV_BACK:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.back(action.payload),
        // navigation.goBack(null),
        state
      );
      break;
    case NAV_START_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'StartScreen' })
          ],
          key: null
        }),
        state
      );
      break;
    case NAV_CREATE_ACC_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'CreateAccountScreen' }),
        state
      );
      break;
    case NAV_PROFILE_SETUP_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'ProfileSetupScreen' })
          ],
          key: null
        }),
        state
      );
      break;
    case NAV_SINGIN_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'SignInScreen' }),
        state
      );
      break;
    case NAV_MANAGE_STD_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'ManageStdStack' })
          ],
          key: 'ManageStdStack'
        }),
        state
      );
      break;
    case NAV_ADD_STD_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'AddStdScreen' }),
        state
      );
      break;
    case NAV_CONFIRM_STD_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ConfirmStdScreen' }),
        state
      );
      break;
    case NAV_ACTIVITIES_SCREEN:
      // StatusBar.setBackgroundColor('red');
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'AppMainStack' })
          ],
          key: null
        }),
        state
      );
      break;
    case NAV_ACTIVITIES_DETAIL_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ActivitiesDetail' }),
        state
      );
      break;
    case NAV_PHOTO_ALMBUMS_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'PhotoAlbumStack' }),
        state
      );
      break;
    case NAV_PHOTO_GALLERY_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'PhotoGallery' }),
        state
      );
      break;
    case NAV_ACT_CALENDAR_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'ActivitiesCalendarScreen' })
          ],
          key: null
        }),
        state
      );
      break;
    case NAV_ATT_REPORT_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'AttendanceReportScreen' }),
        state
      );
      break;
    case NAV_REQ_LEAVE_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'RequestLeaveScreen' }),
        state
      );
      break;
    case NAV_NOTICES_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'NoticesScreen' }),
        state
      );
      break;
    case NAV_CHAT_LIST_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ChatListScreen' }),
        state
      );
      break;
    case NAV_CHAT_SCREEN:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ChatScreen' }),
        state
      );
      break;
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};
