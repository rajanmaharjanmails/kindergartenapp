/**
 * Created by rajanmaharjan on 9/17/17.
 */

import {
  USER_PROFILE_NAME_CHANGED,
  USER_PROFILE_NUM_CHANGED,
  USER_PROFILE_ADDRESS_CHANGED,
  USER_PROFILE_UPDATE,
  USER_PROFILE_UPDATE_SUCCESS,
  USER_PROFILE_UPDATE_FAILED
} from '../../actions/types';

const INITIAL_STATE = {
  user_name: '',
  user_phone: '',
  user_address: '',
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  // console.log('Profile info update', action);
  switch (action.type) {
    case USER_PROFILE_NAME_CHANGED:
      return { ...state, user_name: action.payload, error: '' };
    case USER_PROFILE_NUM_CHANGED:
      return { ...state, user_phone: action.payload, error: '' };
    case USER_PROFILE_ADDRESS_CHANGED:
      return { ...state, user_address: action.payload, error: '' };
    case USER_PROFILE_UPDATE:
      if (!state.user_name || !state.user_phone || !state.user_address) {
        return {
          ...state,
          error: 'Please fill all required fields!',
          loading: false
        };
      }
      return { ...state, loading: true, error: '' };
    case USER_PROFILE_UPDATE_SUCCESS:
      return {
        ...state, ...INITIAL_STATE, loading: false, error: ''
      };
    case USER_PROFILE_UPDATE_FAILED:
      return {
        ...state,
        error: 'Please re-check your submitted information!',
        loading: false
      };
    default:
      return state;
  }
};
