/**
 * Created by rajanmaharjan on 9/17/17.
 */

import {
  STUDENTS_LIST_FETCHING,
  STUDENTS_FETCH_SUCCESS,
  STUDENTS_FETCH_FAILED,
  STUDENT_UNSUBSCRIBE,
  STUDENT_UNSUBSCRIBE_FAILED,
  STUDENT_UNSUBSCRIBE_SUCCESS
} from '../../actions/types';

const INITIAL_STATE = {
  studentData: [],
  dataFetched: false,
  isFetching: false,
  isUnsubscribing: false,
  error: false
};

export default (state = INITIAL_STATE, action) => {
  // console.warn('Student list', action);
  switch (action.type) {
    case STUDENTS_LIST_FETCHING:
      return {
        ...state,
        isFetching: true
      };
    case STUDENTS_FETCH_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        isFetching: false,
        studentData: action.payload.data,
        dataFetched: true
      };
    case STUDENTS_FETCH_FAILED:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case STUDENT_UNSUBSCRIBE:
      return {
        ...state,
        isUnsubscribing: true,
        error: true
      };
    case STUDENT_UNSUBSCRIBE_FAILED:
      return {
        ...state,
        isUnsubscribing: false,
        error: true
      };
    case STUDENT_UNSUBSCRIBE_SUCCESS:
      return {
        ...state,
        isUnsubscribing: false,
        error: false
      };
    default:
      return state;
  }
};
