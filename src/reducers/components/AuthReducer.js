/**
 * Created by rajanmaharjan on 9/11/17.
 */
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  LOGIN_USER
} from '../../actions/types';

const INITIAL_STATE = {
  email: '',
  password: '',
  user: null,
  error: null,
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  // console.log('action', action);
  switch (action.type) {
    case EMAIL_CHANGED:
      return { ...state, email: action.payload, error: '' };
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload, error: '' };
    case LOGIN_USER:
      if (!state.email || !state.password) {
        return {
          ...state,
          error: 'Please enter your credentials!',
          password: '',
          loading: false
        };
      }
      return { ...state, loading: true, error: '' };
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        user: action.payload
      };
    case LOGIN_USER_FAILED:
      return {
        ...state,
        error: 'Please re-check your credentials!',
        password: '',
        loading: false
      };
    default:
      return state;
  }
};
