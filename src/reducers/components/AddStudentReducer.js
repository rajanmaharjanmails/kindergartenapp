/**
 * Created by rajanmaharjan on 9/18/17.
 */

import {
  ADD_STUDENT_CODE_CHANGED,
  STUDENT_CODE_USE,
  STUDENT_CODE_USE_SUCCESS,
  STUDENT_CODE_USE_FAILED,
  STUDENT_CODE_USE_CONFIRM,
  STUDENT_CODE_USE_CONFIRM_SUCCESS,
  STUDENT_CODE_USE_CONFIRM_FAILED,
  STUDENT_CODE_USE_CANCEL
} from '../../actions/types';

const INITIAL_STATE = {
  student_code: '',
  studentDetail: null,
  error: null,
  loading: false,
  student_code_use: true,
  student_code_confirm: true
};

export default (state = INITIAL_STATE, action) => {
  // console.log('action', action);
  switch (action.type) {
    case ADD_STUDENT_CODE_CHANGED:
      return { ...state, student_code: action.payload, error: '' };
    case STUDENT_CODE_USE:
      if (!state.student_code) {
        return {
          ...state,
          error: 'Please enter your student code!',
          loading: false
        };
      }
      return {
        ...state,
        loading: true,
        error: '',
        student_code_use: true,
        student_code_confirm: false
      };
    case STUDENT_CODE_USE_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        studentDetail: action.payload.data,
        student_code: action.payload.studentCode,
        loading: false,
        error: ''
      };
    case STUDENT_CODE_USE_FAILED:
      return {
        ...state,
        error: 'No student found with given code!',
        loading: false,
        student_code_confirm: false
      };
    case STUDENT_CODE_USE_CONFIRM:
      return {
        ...state,
        error: '',
        loading: true,
        student_code_use: false,
        student_code_confirm: true
      };
    case STUDENT_CODE_USE_CONFIRM_SUCCESS:
      return {
        ...state,
        error: '',
        student_code: '',
        loading: false,
        student_code_use: false,
        student_code_confirm: true
      };
    case STUDENT_CODE_USE_CONFIRM_FAILED:
      return {
        ...state,
        error: 'Error while adding student, please try later!',
        loading: false,
        student_code_use: false,
        student_code_confirm: true
      };
    case STUDENT_CODE_USE_CANCEL:
      return {
        ...state,
        error: '',
        loading: false,
        student_code: action.payload,
        student_code_use: false,
        student_code_confirm: true
      };
    default:
      return state;
  }
};
