/**
 * Created by rajanmaharjan on 7/29/17.
 */

import { combineReducers } from 'redux';
import AuthReducer from './components/AuthReducer';
import CreateAccReducer from './components/CreateAccReducer';
import ProfileSetupReducer from './components/ProfileSetupReducer';
import StudentListReducer from './components/StudentListReducer';
import AddStudentReducer from './components/AddStudentReducer';
import ActivitiesCalendarReducer from './components/ActivitiesCalendarReducer';
import AttendanceReportReducer from './components/AttendanceReportReducer';
import LeaveRequestReducer from './components/LeaveRequestReducer';
import NoticesReducer from './components/NoticesReducer';
import AppNavigationReducer from './components/NavigationReducer';

export default combineReducers({
  auth: AuthReducer,
  nav: AppNavigationReducer,
  createUser: CreateAccReducer,
  profileUpdate: ProfileSetupReducer,
  studentList: StudentListReducer,
  addStudent: AddStudentReducer,
  calendarActivities: ActivitiesCalendarReducer,
  attendanceReports: AttendanceReportReducer,
  leaveRequests: LeaveRequestReducer,
  notices: NoticesReducer
});
