/**
 * Created by rajanmaharjan on 7/29/17.
 */
import React, { Component } from 'react';
import {
  View,
  LayoutAnimation
} from 'react-native';

import { Provider } from 'react-redux';
import cssV from './styles/variables/cssV';

import AppWithNavigationState from './navigators/';
import appStore from './store/';
import { OfflineNotice } from './components/1common';

import { setCustomText, setCustomTextInput } from 'react-native-global-props';

const customTextProps = {
  style: {
    fontFamily: cssV('GLOBAL_TEXT_FONT'),
    color: cssV('PRIMARY_WHITE')
  }
};

const customTextInputProps = {
  underlineColorAndroid: 'rgba(0,0,0,0)'
};

setCustomText(customTextProps);
setCustomTextInput(customTextInputProps);

// const ConsolePanel = require('react-native-console-panel/keep-yellowbox')
//   .displayWhenDev();

class KindergartenApp extends Component {
  componentWillUpdate() {
    LayoutAnimation.spring();
  }

  render() {
    return (
      <Provider store={appStore}>
        <View style={{ flex: 1, backgroundColor: 'transparent' }}>
          <AppWithNavigationState />
          <OfflineNotice />
          {/* {ConsolePanel} */}
        </View>
      </Provider>
    );
  }
}

export default KindergartenApp;
