/**
 * Returns app styles
 * @returns {{app styles object}}
 */

import cssV from './variables/cssV';

import {
  Platform, StatusBar,
} from 'react-native';

import StartScreenStyles from '../components/2StartScreen/StartScreenStyles';
import CreateAccScreenStyles from '../components/3CreateAccScreen/CreateAccScreenStyles';
import ProfileSetupScreenStyles from '../components/4ProfileSetupScreen/ProfileSetupStyle';
import SingInScreenStyles from '../components/5SignInScreen/SignInScreenStyles';
import ManageStdScreenStyles from '../components/6ManageStudentScreen/ManageStdScreenStyles';
import AddStdScreenStyles from '../components/7AddStudentScreen/AddStdScreenStyles';
import ConfirmStdScreenStyles from '../components/8ConfirmStudentScreen/ConfirmStdScreenStyles';
import ActivitiesScreenStyles from '../components/9ActivitiesScreen/ActivitiesScreenStyles';
import ActivitiesDetailScreenStyles from '../components/9ActivitiesScreen/ActivitiesDetailScreenStyles';
import PhotoAlbumScreenStyles from '../components/10PhotoAlbumScreen/PhotoAlbumStyles';
import PhotoGalleryScreenStyles from '../components/10PhotoAlbumScreen/PhotoGalleryScreenStyles';
import ActivitiesCalendarScreenStyles from '../components/11ActivitiesCalendarScreen/ActivitiesCalendarStyles';
import AttendanceCalScreenStyles from '../components/12AttendanceReportScreen/AttendanceReportStyle';
import LeaveRequestScreenStyles from '../components/13RequestLeaveScreen/RequestLeaveStyles';
import NoticeScreenStyles from '../components/14NoticesScreen/NoticesScreenStyles';
import ChatListScreenStyles from '../components/15ChatScreen/ChatListScreenStyles';
import ChatScreenStyles from '../components/15ChatScreen/ChatScreenStyles';

const styles = () => {
  const mainStyles = {
    mainContainerStyle: {
      flex: 1,
    },
    errorTextStyle: {
      fontSize: 20,
      alignSelf: 'center',
      color: 'red',
    },
    pickerTextStyle: {
      fontSize: 18,
      paddingLeft: 20,
    },
  };

  const headerStyles = {
    headerStyle: {
      backgroundColor: 'rgba(0,0,0,0)',
      shadowOpacity: 0,
      position: 'absolute',
      top: (Platform.OS === 'ios' ? 0 : StatusBar.currentHeight),
      left: 0,
      right: 0,
    },
    headerCustomStyle: {
      backgroundColor: 'rgba(0,0,0,0)',
      shadowOpacity: 0,
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      height: 75,
    },
    headerTitleStyle: {
      color: '#fefefe',
      fontWeight: '300',
      alignSelf: 'center',
      marginLeft: Platform.OS === 'ios' ? 0 : -30,
      // fontFamily: 'KidsBook',
    },
  };

  return Object.assign(
    {},
    mainStyles,
    headerStyles,
    StartScreenStyles(),
    CreateAccScreenStyles(),
    ProfileSetupScreenStyles(),
    SingInScreenStyles(),
    ActivitiesScreenStyles(),
    ActivitiesDetailScreenStyles(),
    PhotoAlbumScreenStyles(),
    PhotoGalleryScreenStyles(),
    ManageStdScreenStyles(),
    AddStdScreenStyles(),
    ConfirmStdScreenStyles(),
    ActivitiesCalendarScreenStyles(),
    AttendanceCalScreenStyles(),
    LeaveRequestScreenStyles(),
    NoticeScreenStyles(),
    ChatListScreenStyles(),
    ChatScreenStyles()
  );
};

export default styles;
