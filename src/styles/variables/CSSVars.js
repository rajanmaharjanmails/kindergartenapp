module.exports = {
  /*
   * Color variables
   * */
  DARK_TEAL: '#026188',
  LIGHT_TEAL: '#1689a8',
  SKY_BLUE:'#d8f9f4',
  LIGHT_BLUE:'#009fde',
  PRIMARY_BLUE: '#2889df',
  PRIMARY_WHITE: '#fff',
  STATUS_BAR_COLOR: 'rgba(1, 79, 112, 0.8)',
  SHADOW_COLOR: '#595f59',
  CUSTOM_GRAY:'#797979',
  LIGHT_GRAY: '#8e9091',
  EX_LIGHT_GRAY: '#cbcbcb',
  DARK_GRAY: '#656768',
  EX_DARK_GRAY: '#444647',
  EXL_BLACK: '#2b2b2b',
  LIGHT_BLACK: '#000',
  LIGHT_WHITE: '#ddd',
  RED:'#c1250d',
  ORANGE:'#e0a20d',
  GREEN:'#009200',
  WHITE: '#fff',
  BLUE: '#007aff',
  /*
   * Font variables
   * */
  GLOBAL_TEXT_FONT: 'Sniglet-Regular',
  INPUT_TEXT_FONT: 'LittleMissPriss',
  CUSTOM_TEXT_FONT: 'LittleMissPriss',
  /*
   * Drawer items
   * */
  DRAWER_ICON_SIZE: 20,
};
