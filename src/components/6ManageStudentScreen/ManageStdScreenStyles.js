/**
 * Created by rajanmaharjan on 8/31/17.
 */
import cssV from '../../styles/variables/cssV';

const ManageStdScreenStyles = () => {
  return {
    manageStdScreenContainer: {
      flexGrow: 1,
    },
    manageStdScreenViewContainer: {
      flex: 1,
      marginTop: 55,
    },
    manageStdScreenCardSection: {
      flex: 1,
      paddingLeft: 15,
      paddingRight: 15,
      marginTop: 15,
    },
    manageStdScreenErrorCardSection: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      paddingLeft: 15,
      paddingRight: 15,
      marginBottom: 100,
    },
    manageStdCardContainer: {
      flex: 1,
      flexDirection: 'row',
      marginBottom: 7,
    },
    manageStdImageContainer: {
      width: 55,
      height: 55,
      backgroundColor: 'transparent',
      borderRadius: 25,
      resizeMode: 'cover',
      marginRight: 10,
      marginTop: 5,
      marginLeft: 5,
    },
    manageStdDataContainer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      paddingLeft: 5,
      paddingTop: 5,
    },
    manageStdDataTitle: {
      fontSize: 18,
      fontWeight: '400',
      color: cssV('DARK_GRAY'),
    },
    manageStdDataContent: {
      color: cssV('EX_DARK_GRAY'),
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      fontSize: 20,
      paddingTop: 7,
      fontWeight: '400',
    },
    manageStdIconContainer: {
      justifyContent: 'center',
    },
    manageStdIconStyle: {
      color: cssV('LIGHT_GRAY'),
    },
    manageStdActionButton: {
      marginBottom: 40,
    },
    manageStdActionButtonShadow: {
      shadowColor: cssV('SHADOW_COLOR'),
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.4,
      shadowRadius: 2,
      elevation: 3,
    },
    manageStdErrorText: {
      fontSize: 18,
      color: cssV('LIGHT_TEAL'),
      textAlign: 'center',
    },
    manageStdRefreshButton: {
      marginTop: 30,
      backgroundColor: cssV('LIGHT_TEAL'),
      borderColor: cssV('LIGHT_TEAL'),
    },
    menuOptionPaddingStyle: {
      borderTopWidth: 1,
      borderColor: cssV('PRIMARY_WHITE'),
    },
    menuOptionTextStyle: {
      paddingLeft: 20,
      fontSize: 15,
      color: cssV('PRIMARY_WHITE'),
    },
    manageStdNotSelectedStd: {
      backgroundColor: 'transparent',
    },
    manageStdSelectedStd: {
      backgroundColor: cssV('PRIMARY_WHITE'),
    },
    manageStdListSeperator: { height: 7 }
  };
};

export default ManageStdScreenStyles;