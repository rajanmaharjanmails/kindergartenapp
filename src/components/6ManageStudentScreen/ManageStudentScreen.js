/**
 * Created by rajanmaharjan on 8/31/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  StyleSheet,
  Platform,
  View,
  Text,
  Image,
  FlatList,
  Alert,
  TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ActionButton from 'react-native-action-button';
import MyText from 'react-native-letter-spacing';
import session from '../../utils/UserSession';
import Spinner from 'react-native-loading-spinner-overlay';

import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-menu';

import {
  MyStatusBar,
  BackgroundImageContainer,
  BarButton,
  CardSection,
  ProfileImageComponent,
  DefaultBackButton,
  CustomButton
} from '../1common';

import {
  NAV_ADD_STD_SCREEN
} from '../../actions/types';

import {
  BUTTON_DEFAULT,
  BUTTON_CUSTOM_HEADER_BACK
} from '../1common/Button/ButtonTypes';

import {
  getStudentList, selectStudent, unsubscribeStudent
} from '../../actions';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class ManageStdScreen extends Component {

  constructor() {
    super();

    this.state = {
      selectedStudentId: null
    };

    this._renderStudents = this._renderStudents.bind(this);
    this._getStudentList = this._getStudentList.bind(this);
    this._onItemSelected = this._onItemSelected.bind(this);
    this._unsubscribeStudent = this._unsubscribeStudent.bind(this);
    this._hightligtCurrentSelected = this._hightligtCurrentSelected.bind(this);
    this.session = new session.Session();

  }

  _getStudentList() {
    this.props.getStudentList();
    this._hightligtCurrentSelected();
  }

  componentDidMount() {
    this._getStudentList();
  }

  _renderStudentImage = (photoUrl) => {
    const { manageStdImageContainer } = styles;

    if (photoUrl && photoUrl.thumb)
      return (
        <Image
          source={{ uri: photoUrl.thumb }}
          style={manageStdImageContainer}
        />
      );

    return (
      <Image
        source={require('../../assets/icons/default_user_icon_db.png')}
        style={manageStdImageContainer}
      />
    );
  };

  _onItemSelected = (item) => {
    this.props.selectStudent(item.id);
    this.setState({
      selectedStudentId: item.id
    });
  }

  _hightligtCurrentSelected = () => {
    this.session.getStudentId().then((resp) => {
      this.setState({
        selectedStudentId: resp
      });
    });
  }

  _unsubscribeStudent = (studentId) => {
    this.props.unsubscribeStudent(studentId);
    this._getStudentList();
  }

  _renderStudents = (item) => {

    const {
      manageStdCardContainer,
      manageStdDataContainer,
      manageStdDataTitle,
      manageStdDataContent,
      manageStdIconContainer,
      manageStdIconStyle,
      menuOptionTextStyle,
      // menuOptionPaddingStyle,
      manageStdSelectedStd,
      manageStdNotSelectedStd
    } = styles;

    const studentClass = 'Class: ' + item.class;
    this._hightligtCurrentSelected;

    return (

      <TouchableHighlight
        underlayColor={cssV('EX_LIGHT_GRAY')}
        activeOpacity={10}
        style={this.state.selectedStudentId === item.id ? manageStdSelectedStd : manageStdNotSelectedStd}
        onPress={() => this._onItemSelected(item)}>
        <View style={manageStdCardContainer}>
          <ProfileImageComponent>
            {this._renderStudentImage(item.photo_url)}
          </ProfileImageComponent>

          <View style={manageStdDataContainer}>
            <Text
              style={manageStdDataTitle}>
              {item.name}
            </Text>
            <MyText
              letterSpacing={1}
              wordSpacing={5}
              style={manageStdDataContent}
            >
              {studentClass}
            </MyText>
          </View>
          <View style={manageStdIconContainer}>
            <Menu
              onSelect={(value) => {
                // console.log(value, item);
                if (value === 1) {
                  Alert.alert('Remove Student?',
                    `Are you sure you want to unsubscribe ${JSON.stringify(item.name)}?`,
                    [
                      { text: 'Cancel', onPress: () => null },
                      { text: 'OK', onPress: () => this._unsubscribeStudent(item.id) },
                    ]
                  );
                }
              }}>
              <MenuTrigger>
                <Icon name='dots-vertical'
                  size={25}
                  style={manageStdIconStyle}
                />
              </MenuTrigger>
              <MenuOptions
                optionsContainerStyle={{ backgroundColor: cssV('STATUS_BAR_COLOR') }}>
                <MenuOption value={1}>
                  <Text style={menuOptionTextStyle}>Remove account</Text>
                </MenuOption>
              </MenuOptions>
            </Menu>
          </View>
        </View>

      </TouchableHighlight>
    );
  };

  _keyExtractor = (item, index) => item.id;

  _onRefreshButtonPress() {
    this._getStudentList();
  }

  render() {
    const {
      navigation, isFetching, studentData, error, isUnsubscribing
    } = this.props;

    const {
      manageStdScreenContainer,
      manageStdScreenViewContainer,
      manageStdScreenCardSection,
      manageStdScreenErrorCardSection,
      manageStdActionButton,
      manageStdActionButtonShadow,
      manageStdErrorText,
      manageStdRefreshButton,
      manageStdListSeperator
    } = styles;

    const studentsDetails = (error && !isFetching && !isUnsubscribing && !studentData.length) || (!error && !isFetching && !isUnsubscribing && !studentData.length) ? (
      <CardSection
        style={manageStdScreenErrorCardSection}>
        <Text style={manageStdErrorText}>
          Error fetching student list.
        </Text>
        <CustomButton
          onPress={this._onRefreshButtonPress.bind(this)}
          buttonType={BUTTON_DEFAULT}
          style={manageStdRefreshButton}
        >
          Refresh
        </CustomButton>
      </CardSection>
    ) : (
        <CardSection style={manageStdScreenCardSection}>
          <FlatList
            data={studentData}
            renderItem={({ item }) => this._renderStudents(item)}
            extraData={this.state}
            ItemSeparatorComponent={() => <View style={manageStdListSeperator} />}
            keyExtractor={this._keyExtractor}
            onRefresh={() => this._getStudentList()}
            refreshing={isFetching}
          />
        </CardSection>
      );

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MenuContext style={{ flex: 1 }}>
          <MyStatusBar backgroundColor={cssV('STATUS_BAR_COLOR')}
            barStyle="light-content" />
          <Spinner
            visible={isUnsubscribing}
            animation='fade'
            overlayColor="rgba(1, 0, 10, 0.9)"
            textContent={'Removing account...'}
            textStyle={{
              color: '#FFF',
              fontWeight: '300',
            }} />
          <View
            style={manageStdScreenContainer}>
            <View
              style={manageStdScreenViewContainer}>
              {studentsDetails}
            </View>
          </View>
          <ActionButton
            buttonColor={cssV('DARK_TEAL')}
            onPress={() => navigation.dispatch({ type: NAV_ADD_STD_SCREEN })}
            position="right"
            backdrop={true}
            shadowStyle={manageStdActionButtonShadow}
            style={manageStdActionButton}
            icon={
              <Icon name='plus'
                size={30}
                style={{ color: cssV('PRIMARY_WHITE') }}
              />
            }
          />
        </MenuContext>
      </BackgroundImageContainer>
    );
  }
}

ManageStdScreen.navigationOptions = ({ navigation }) => ({
  title: 'Manage Students',
  headerTitleStyle: {
    color: cssV('PRIMARY_WHITE'),
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
    // paddingLeft: (Platform.OS === 'ios' ? 0 : 40),
  },
  headerStyle: {
    backgroundColor: cssV('DARK_TEAL'),
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
    buttonType={BUTTON_CUSTOM_HEADER_BACK} />,
  headerRight: <BarButton navigation={navigation} />,
  drawerLabel: 'Students',
  drawerIcon: ({ tintColor }) => (
    <Icon name='account-switch'
      size={cssV('DRAWER_ICON_SIZE')}
      style={{ color: tintColor }}
    />
  ),
});

const mapStateToProps = ({ studentList }) => {
  const { studentData, isFetching, isUnsubscribing, error } = studentList;
  return { studentData, isFetching, isUnsubscribing, error };
};

export default connect(mapStateToProps, { getStudentList, selectStudent, unsubscribeStudent })(
  ManageStdScreen);