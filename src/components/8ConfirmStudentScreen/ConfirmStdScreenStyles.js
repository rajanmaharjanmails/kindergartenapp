/**
 * Created by rajanmaharjan on 8/31/17.
 */
import cssV from '../../styles/variables/cssV';

const ConfirmStdScreenStyles = () => {
  return {
    confirmStdScreenContainer: {
      flexGrow: 1,
    },
    confirmStdScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 50,
    },
    confirmStdScreenCustomBtn: {
      marginTop: 40,
      backgroundColor: cssV('LIGHT_TEAL'),
      borderColor: cssV('LIGHT_TEAL'),
    },
    confirmStdScreenCustomBtnCancel: {
      marginTop: 10,
      backgroundColor: cssV('LIGHT_TEAL'),
      borderColor: cssV('LIGHT_TEAL'),
    },
    confirmStdScreenProfileImg: {
      width: 140,
      height: 140,
      backgroundColor: 'transparent',
      borderRadius: 70,
      resizeMode: 'cover',
      marginRight: 10,
    },
    confirmStdScreenTextName: {
      fontSize: 20,
      fontWeight: '400',
      color: cssV('DARK_GRAY'),
      marginTop: 20,
      marginBottom: 10,
    },
    confirmStdScreenTextGrade: {
      color: cssV('EX_DARK_GRAY'),
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      fontSize: 20,
      fontWeight: '400',
    },
  };
};

export default ConfirmStdScreenStyles;
