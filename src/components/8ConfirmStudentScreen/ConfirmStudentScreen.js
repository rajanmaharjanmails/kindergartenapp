/**
 * Created by rajanmaharjan on 8/31/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  StyleSheet,
  Platform,
  View,
  Text,
  Image,
  Alert,
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MyText from 'react-native-letter-spacing';
import Spinner from 'react-native-loading-spinner-overlay';

import {
  MyStatusBar,
  BackgroundImageContainer,
  BarButton,
  DefaultBackButton,
  CustomButton,
  ProfileImageComponent,
} from '../1common';

import {
  BUTTON_DEFAULT,
  BUTTON_CUSTOM_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import {
  confirmStudentCode,
  useStudentCodeCancel,
} from '../../actions';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class ConfirmStdScreen extends Component {

  onConfirmButtonPress () {
    const {student_code} = this.props;
    this.props.confirmStudentCode({student_code});
  }

  onCancelButtonPress () {
    const {student_code} = this.props;
    this.props.useStudentCodeCancel(student_code);
  }

  render () {
    const {studentDetail, student_code_confirm, error, loading} = this.props;

    const {
      confirmStdScreenContainer,
      confirmStdScreenViewContainer,
      confirmStdScreenCustomBtn,
      confirmStdScreenCustomBtnCancel,
      confirmStdScreenProfileImg,
      confirmStdScreenTextName,
      confirmStdScreenTextGrade,
    } = styles;

    const studentClass = 'Grade: ' + studentDetail.class;
    const studentImage = (studentDetail.photo_url &&
    studentDetail.photo_url.full) ? (
      <Image
        source={require('../../assets/images/child.png')}
        style={
          confirmStdScreenProfileImg
        }
      />
    ) : (
      <Image
        source={require('../../assets/icons/default_user_icon_lb.png')}
        style={confirmStdScreenProfileImg}
      />
    );

    if (error && student_code_confirm)
      Alert.alert('Error!', error);

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={cssV('STATUS_BAR_COLOR')}
                     barStyle="light-content"/>
        <Spinner
          visible={loading && student_code_confirm && !error}
          animation='fade'
          overlayColor="rgba(1, 0, 10, 0.9)"
          textContent={'Adding student...'}
          textStyle={{
            color: '#FFF',
            fontWeight: '300',
          }}/>
        <KeyboardAwareScrollView
          contentContainerStyle={confirmStdScreenContainer}>
          <View style={confirmStdScreenViewContainer}>
            <ProfileImageComponent>
              {studentImage}
            </ProfileImageComponent>
            <Text style={confirmStdScreenTextName}>
              {studentDetail.name}
            </Text>
            <MyText
              letterSpacing={1}
              wordSpacing={5}
              style={confirmStdScreenTextGrade}
            >
              {studentClass}
            </MyText>
            <CustomButton
              onPress={this.onConfirmButtonPress.bind(this)}
              buttonType={BUTTON_DEFAULT}
              style={confirmStdScreenCustomBtn}
            >
              Confirm
            </CustomButton>
            <CustomButton
              onPress={this.onCancelButtonPress.bind(this)}
              buttonType={BUTTON_DEFAULT}
              style={confirmStdScreenCustomBtnCancel}
            >
              Cancel
            </CustomButton>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

ConfirmStdScreen.navigationOptions = ({navigation}) => ({
  title: 'Confirm Student',
  headerTitleStyle: {
    color: cssV('PRIMARY_WHITE'),
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
  },
  headerStyle: {
    backgroundColor: cssV('DARK_TEAL'),
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_CUSTOM_HEADER_BACK}/>,
  headerRight: <BarButton navigation={navigation}/>,
  drawerLabel: 'Accounts',
  drawerIcon: ({tintColor}) => (
    <Icon name='account-switch'
          size={cssV('DRAWER_ICON_SIZE')}
          style={{color: tintColor}}
    />
  ),
});

const mapStateToProps = ({addStudent}) => {
  const {student_code, student_code_confirm, error, loading, studentDetail} = addStudent;
  return {student_code, student_code_confirm, error, loading, studentDetail};
};

export default connect(mapStateToProps, {
  confirmStudentCode, useStudentCodeCancel,
})(ConfirmStdScreen);