/**
 * Created by rajanmaharjan on 9/2/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  StyleSheet,
  Platform,
  View,
  Text,
  Alert,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Calendar } from 'react-native-calendars';
import MyText from 'react-native-letter-spacing';
import moment from 'moment';
import Moment from 'moment-weekdaysin';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';

import {
  MyStatusBar,
  BackgroundImageContainer,
  CommonInput,
  BarButton,
  DefaultBackButton,
  Card,
  CardSection,
  CustomButton,
} from '../1common';

import {
  BUTTON_CUSTOM_HEADER_BACK,
  BUTTON_DEFAULT,
} from '../1common/Button/ButtonTypes';

import {
  getLeaveRequests,
  leaveRequestsConfirm,
  leaveRequestDateChanged,
  leaveRequestDaysNoChanged,
  leaveRequestReasonChanged,
} from '../../actions';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

const calendarTheme = {
  calendarBackground: cssV('DARK_TEAL'),
  textSectionTitleColor: cssV('LIGHT_WHITE'),
  selectedDayBackgroundColor: 'red',
  selectedDayTextColor: cssV('LIGHT_TEAL'),
  todayTextColor: '#00adf5',
  dayTextColor: cssV('LIGHT_WHITE'),
  textDisabledColor: cssV('LIGHT_GRAY'),
  dotColor: '#00adf5',
  selectedDotColor: 'red',
  arrowColor: cssV('LIGHT_WHITE'),
  monthTextColor: cssV('LIGHT_WHITE'),
  textDayFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textMonthFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textDayHeaderFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textDayFontSize: 16,
  textMonthFontSize: 20,
  textDayHeaderFontSize: 16,
};

class RequestLeaveScreen extends Component {
  constructor () {
    super();

    this.state = {
      currentDate: moment().format('YYYY-MM-DD'),
      currentMonth: moment().month() + 1,
      currentYear: moment().year(),
      isDateTimePickerVisible: false,
    };

    this._getMarkedDays = this._getMarkedDays.bind(this);
    this._renderCalendar = this._renderCalendar.bind(this);
    this._getLeaveRequests = this._getLeaveRequests.bind(this);
    this._showErrorDialog = this._showErrorDialog.bind(this);
  }

  _getLeaveRequests (month, year) {
    let currentMonth = month ? month : this.state.currentMonth;
    let currentYear = year ? year : this.state.currentYear;
    this.props.getLeaveRequests(currentYear, currentMonth);
  }

  _showErrorDialog () {
    Alert.alert(
      'Request failed!',
      'Leave request not registered, please try later!',
    );
  }

  componentDidMount () {
    this._getLeaveRequests();
  }

  renderArrow = (direction) => {
    let iconName = '';
    if (direction === 'left')
      iconName = 'chevron-left';
    else
      iconName = 'chevron-right';

    return (
      <Icon name={iconName}
            size={25}
            style={{
              color: cssV('LIGHT_WHITE'),
            }}
      />);
  };

  _showDateTimePicker = () => this.setState({isDateTimePickerVisible: true});

  _hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});

  _handleDatePicked = (date) => {
    const dateValidate = moment(moment().format('YYYY-MM-DD')).
      diff(moment(date).format('YYYY-MM-DD'));
    if (parseInt(dateValidate) > 0)
      Alert.alert(
        'Invalid date',
        'Date must be future or current day.',
      );
    else
      this.props.leaveRequestDateChanged(moment(date).format('YYYY-MM-DD'));
    this._hideDateTimePicker();
  };

  _renderLeaveDetails = () => {

    let {leaveRequestsData, isFetching} = this.props;

    leaveRequestsData = leaveRequestsData.length ? leaveRequestsData : [];

    let leaveDays = '';

    const {
      leaveRequestSummaryCard,
      leaveRequestTitleStyle,
      leaveRequestDetailStyle,
      leaveRequestSummaryLoading,
      leaveRequestSummaryLoadingText,
    } = styles;

    if (isFetching)
      return (
        <CardSection
          style={leaveRequestSummaryLoading}>
          <Text style={leaveRequestSummaryLoadingText}>
            Updating details...
          </Text>
        </CardSection>
      );

    leaveRequestsData.map((item) => {
      let leaveMonth = moment(item.date).month() + 1;

      if (leaveMonth === this.state.currentMonth) {
        if (parseInt(item.no_of_days) > 1) {
          leaveDays = leaveDays + ` ${moment(item.date).date()},`;
          for (let i = 1; i < parseInt(item.no_of_days); i++)
            leaveDays = leaveDays + `  ${moment(item.date).date() + i},`;
        } else {
          leaveDays = leaveDays + `  ${moment(item.date).date()},`;
        }
      } else {
        leaveDays = null;
      }
    });

    return (
      <Card
        style={leaveRequestSummaryCard}>
        <Text style={leaveRequestTitleStyle}>
          Leave this month
        </Text>
        <MyText
          letterSpacing={2}
          wordSpacing={5}
          style={leaveRequestDetailStyle}>
          {leaveDays
            ? leaveDays.substring(0, leaveDays.length - 1)
            : 'No leaves requests'}
        </MyText>
      </Card>
    );
  };

  onLeaveRequestDateChanged (text) {
    this.props.leaveRequestDateChanged(text);
  }

  onLeaveRequestDaysChanged (text) {
    this.props.leaveRequestDaysNoChanged(text);
  }

  onLeaveRequestReasonChanged (text) {
    this.props.leaveRequestReasonChanged(text);
  }

  onRequestButtonPress () {
    const {requestLeaveDate, requestLeaveDaysNo, requestLeaveReason} = this.props;

    if (!requestLeaveDate || !requestLeaveDaysNo || !requestLeaveReason)
      return Alert.alert('Request Failed!', 'Please fill all required fields!');

    this.props.leaveRequestsConfirm(requestLeaveDate, requestLeaveDaysNo,
      requestLeaveReason);

    setTimeout(() => {
      this._getLeaveRequests();
    }, 3000);
  }

  _renderRequestLeaveForm = () => {
    const {
      leaveRequestFormCard,
      leaveRequestTitleStyle,
      leaveRequestInputStyle,
      leaveRequestScreenCustomBtn,
      leaveRequestFormTextInput,
      leaveRequestFormTextInputIcon,
    } = styles;

    const {
      requestLeaveDate, requestLeaveDaysNo, requestLeaveReason, reqLeaveError, reqLeaveMsg,
    } = this.props;

    if (reqLeaveError && !reqLeaveMsg)
      this._showErrorDialog();

    return (
      <Card
        style={leaveRequestFormCard}>
        <Text style={leaveRequestTitleStyle}>
          Request Leave
        </Text>
        <TouchableOpacity
          onPress={this._showDateTimePicker}
          // hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
        >
          <View pointerEvents={'none'}>
            <CommonInput
              placeholder="Select a Date"
              iconName="calendar-check"
              iconColor={cssV('EXL_BLACK')}
              rightIcon={true}
              style={leaveRequestInputStyle}
              onChangeText={this.onLeaveRequestDateChanged.bind(this)}
              textInputStyle={leaveRequestFormTextInput}
              placeholderTextColor={cssV('DARK_GRAY')}
              iconStyle={leaveRequestFormTextInputIcon}
              editable={false}
              value={requestLeaveDate ? moment(requestLeaveDate).
                format('YYYY-MM-DD') : requestLeaveDate }
            />
          </View>
        </TouchableOpacity>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
        <CommonInput
          placeholder="Number of Days"
          iconName="counter"
          iconColor={cssV('EXL_BLACK')}
          rightIcon={true}
          keyboardType="numeric"
          style={leaveRequestInputStyle}
          onChangeText={this.onLeaveRequestDaysChanged.bind(this)}
          textInputStyle={leaveRequestFormTextInput}
          placeholderTextColor={cssV('DARK_GRAY')}
          iconStyle={leaveRequestFormTextInputIcon}
          maxLength={2}
          value={requestLeaveDaysNo}
        />
        <CommonInput
          placeholder="Reason"
          style={leaveRequestInputStyle}
          onChangeText={this.onLeaveRequestReasonChanged.bind(this)}
          textInputStyle={leaveRequestFormTextInput}
          placeholderTextColor={cssV('DARK_GRAY')}
          value={requestLeaveReason}
        />
        <CustomButton
          onPress={this.onRequestButtonPress.bind(this)}
          buttonType={BUTTON_DEFAULT}
          style={leaveRequestScreenCustomBtn}
        >
          Request a leave
        </CustomButton>
      </Card>
    );
  };

  _getMarkedDays = () => {

    let {
      leaveRequestsData, isFetching,
    } = this.props;

    leaveRequestsData = leaveRequestsData.length ? leaveRequestsData : [];

    if (isFetching)
      return null;

    let markedDays = {};
    let SaturdayStyle = [{textColor: cssV('ORANGE')}];
    let LeaveStyle = [
      {startingDay: true, color: cssV('RED'),},
      {endingDay: true, color: cssV('RED')}];

    const Saturdays = Moment().weekdaysInYear('Saturday');
    // const NextYerSaturdays = Moment(moment().add(1, 'y')).
    //   weekdaysInYear('Saturday');

    Saturdays.map((item) => {
      markedDays[moment(item).format('YYYY-MM-DD')] = SaturdayStyle;
    });

    // NextYerSaturdays.map((item) => {
    //   markedDays[moment(item).format('YYYY-MM-DD')] = SaturdayStyle;
    // });

    leaveRequestsData.map((item) => {
      if (parseInt(item.no_of_days) > 1) {
        markedDays[moment(item.date).format('YYYY-MM-DD')] = LeaveStyle;
        for (let i = 1; i < parseInt(item.no_of_days); i++)
          markedDays[moment(item.date).add(i, 'day').
            format('YYYY-MM-DD')] = LeaveStyle;
      }

      markedDays[moment(item.date).format('YYYY-MM-DD')] = LeaveStyle;
    });

    return markedDays;
  };

  _renderCalendar = () => {
    return (
      <Calendar
        current={this.state.currentDate}
        minDate={moment().subtract(6, 'M').format('YYYY-MM-DD')}
        maxDate={moment().add(1, 'y').format('YYYY-MM-DD')}
        // onDayPress={(day) => {console.warn('selected day', day);}}
        monthFormat={'MMMM yyyy'}
        onMonthChange={(selectedDate) => {
          this.setState({
            currentMonth: selectedDate.month,
            currentYear: selectedDate.year,
            currentDate: moment(selectedDate.dateString).format('YYYY-MM-DD'),
          });
          this._getLeaveRequests(selectedDate.month, selectedDate.year);
        }}
        hideArrows={false}
        renderArrow={(direction) => (this.renderArrow(direction))}
        hideExtraDays={false}
        disableMonthChange={false}
        firstDay={0}
        markingType={'interactive'}
        markedDates={this._getMarkedDays()}
        theme={calendarTheme}
      />
    );
  };

  render () {
    const {isFetching, requestingLeave} = this.props;

    const {
      leaveRequestScreenContainer,
      leaveRequestScreenViewContainer,
      leaveRequestCalContainer,
      leaveRequestCardContainer,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={'rgba(1, 79, 112, 0.8)'}
                     barStyle="light-content"/>
        <Spinner
          visible={requestingLeave}
          animation='fade'
          overlayColor="rgba(1, 0, 10, 0.9)"
          textContent={'Leave request...'}
          textStyle={{
            color: '#FFF',
            fontWeight: '300',
          }}/>
        <KeyboardAwareScrollView
          contentContainerStyle={leaveRequestScreenContainer}
          // automaticallyAdjustContentInsets={false}
          keyboardShouldPersistTaps="always"
          style={leaveRequestScreenViewContainer}
          refreshControl={
            <RefreshControl
              refreshing={isFetching}
              onRefresh={this._getLeaveRequests}
            />
          }
        >
          <View style={leaveRequestCalContainer}>
            {this._renderCalendar()}
          </View>
          <View style={leaveRequestCardContainer}>
            {this._renderLeaveDetails()}
            {this._renderRequestLeaveForm()}
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

RequestLeaveScreen.navigationOptions = ({navigation}) => ({
  title: 'Leave Request',
  headerTitleStyle: {
    color: '#fff',
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
  },
  headerStyle: {
    backgroundColor: '#026188',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_CUSTOM_HEADER_BACK}/>,
  headerRight: <BarButton navigation={navigation}/>,
  drawerIcon: ({tintColor}) => (
    <Icon name='calendar-remove'
          size={cssV('DRAWER_ICON_SIZE')}
          style={{color: tintColor}}
    />
  ),
});

const mapStateToProps = ({leaveRequests}) => {
  const {
    leaveRequestsData, dataFetched, isFetching, error,
    requestLeaveDate, requestLeaveDaysNo, requestLeaveReason, reqLeaveError, requestingLeave, requestSuccess, reqLeaveMsg,
  } = leaveRequests;
  return {
    leaveRequestsData,
    dataFetched,
    isFetching,
    error,
    requestLeaveDate,
    requestLeaveDaysNo,
    requestLeaveReason,
    reqLeaveError,
    reqLeaveMsg,
    requestingLeave,
    requestSuccess,
  };
};

export default connect(mapStateToProps, {
  getLeaveRequests,
  leaveRequestsConfirm,
  leaveRequestDateChanged,
  leaveRequestDaysNoChanged,
  leaveRequestReasonChanged,
})(RequestLeaveScreen);