/**
 * Created by rajanmaharjan on 9/2/17.
 */
import {
  Platform,
} from 'react-native';

import cssV from '../../styles/variables/cssV';

const LeaveRequestScreenStyles = () => {
  return {
    leaveRequestScreenContainer: {
      flexGrow: 1,
    },
    leaveRequestScreenViewContainer: {
      flex: 1,
      marginTop: Platform.OS === 'ios' ? 48 : 44,
      marginBottom: 30,
    },
    leaveRequestCalContainer: {
      backgroundColor: cssV('DARK_TEAL'),
      padding: 10,
      paddingBottom: 20,
      paddingTop: 10,
      marginBottom: 0,
    },
    leaveRequestTitleStyle: {
      color: cssV('EXL_BLACK'),
      fontSize: 16,
    },
    leaveRequestDetailStyle: {
      color: cssV('EX_DARK_GRAY'),
      marginTop: 10,
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      fontSize: 18,
      fontWeight: '400',
    },
    leaveRequestInputStyle: {
      borderColor: cssV('LIGHT_GRAY'),
      marginLeft: 5,
      marginRight: 5,
    },
    leaveRequestCardContainer: {
      flex: 1,
    },
    leaveRequestScreenCustomBtn: {
      marginBottom: 30,
      marginTop: 30,
      marginLeft: 5,
      marginRight: 5,
      backgroundColor: cssV('DARK_TEAL'),
      borderColor: cssV('DARK_TEAL'),
    },
    leaveRequestSummaryCard: {
      backgroundColor: cssV('PRIMARY_WHITE'),
      marginTop: 12,
      marginLeft: 20,
      marginRight: 20,
      padding: 10,
    },
    leaveRequestSummaryLoading: {
      marginTop: 12,
      justifyContent: 'center',
      padding: 10,
    },
    leaveRequestSummaryLoadingText: {
      fontSize: 18,
      color: cssV('EX_DARK_GRAY'),
    },
    leaveRequestFormCard: {
      backgroundColor: cssV('PRIMARY_WHITE'),
      marginTop: 10,
      marginBottom: 10,
      marginLeft: 20,
      marginRight: 20,
      padding: 10,
    },
    leaveRequestFormTextInput: {
      fontSize: 22,
      color: cssV('EX_DARK_GRAY'),
      textAlign: 'left',
    },
    leaveRequestFormTextInputIcon: {
      marginTop: 10,
    },
  };
};

export default LeaveRequestScreenStyles;