/**
 * Created by rajanmaharjan on 9/2/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dropdown } from 'react-native-material-dropdown';

import {
  StyleSheet,
  Platform,
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MyText from 'react-native-letter-spacing';

import {
  MyStatusBar,
  BackgroundImageContainer,
  BarButton,
  DefaultBackButton,
  ProfileImageComponent,
  Card,
  CardSection,
} from '../1common';

import {
  BUTTON_CUSTOM_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import {
  getNotices,
} from '../../actions';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class NoticesScreen extends Component {

  constructor () {
    super();

    this.state = {
      selectedFilterItem: null,
      renderNoticeData: [],
    };

    this._getNotices = this._getNotices.bind(this);
    this._renderNoticeList = this._renderNoticeList.bind(this);
  };

  _getNotices () {
    this.setState({
      selectedFilterItem: 'ALL',
    });
    this.props.getNotices();
  }

  componentDidMount () {
    this._getNotices();
  }

  _renderNotices = (item) => {

    const {
      noticeScreeListViewContainer,
      noticeScreenImageContainer,
      noticeScreenImageStyle,
      noticeScreenDataContainer,
      noticeScreenDataTitle,
      noticeScreenDataContent,
      noticeScreenDataContentContainer,
      noticeScreenTimeContainer,
      noticeScreenTimeStyle,
    } = styles;

    return (
      <View style={noticeScreeListViewContainer}>
        <ProfileImageComponent
          style={noticeScreenImageContainer}>
          <Image
            source={require('../../assets/icons/soccer.png')}
            style={noticeScreenImageStyle}
          />
        </ProfileImageComponent>
        <View style={noticeScreenDataContainer}>
          <Text
            style={noticeScreenDataTitle}>
            {item.title}
          </Text>
          <MyText
            letterSpacing={1}
            wordSpacing={5}
            style={noticeScreenDataContent}
            containerStyle={noticeScreenDataContentContainer}
          >
            {item.description}
          </MyText>
        </View>
        {/*<View style={noticeScreenTimeContainer}>*/}
        {/*<Text*/}
        {/*style={noticeScreenTimeStyle}>*/}
        {/*{item.time}*/}
        {/*</Text>*/}
        {/*</View>*/}
      </View>
    );
  };

  _renderNoticeList = () => {
    const {noticesData, isFetching} = this.props;

    const {
      noticeScreeListLoading,
      noticeScreeListLoadingText,
    } = styles;

    if (isFetching)
      return (
        <CardSection
          style={noticeScreeListLoading}>
          <ActivityIndicator size="large"/>
          <Text style={noticeScreeListLoadingText}>
            Updating notices...
          </Text>
        </CardSection>
      );

    return (
      <FlatList
        data={(this.state.selectedFilterItem &&
        this.state.selectedFilterItem !== 'ALL')
          ? this.state.renderNoticeData
          : noticesData}
        extraData={this.state.renderNoticeData}
        renderItem={({item}) => this._renderNotices(item)}
        keyExtractor={this._keyExtractor}
        onRefresh={() => this._getNotices()}
        refreshing={isFetching}
      />
    );
  };

  _keyExtractor = (item, index) => item.id;

  render () {
    const {noticesData} = this.props;

    let filterItems = [
      {
        value: 'ALL',
      }];

    if (noticesData.length) {
      const uniqueFilterItems = [
        ...new Set(noticesData.map(item => item.type))];

      uniqueFilterItems.map((item) => {
        filterItems.push({value: item});
      });
    }

    const {
      noticeScreenViewContainer,
      noticeScreenListCard,
      noticeScreenDropdownContainer,
      noticeScreenDropdownPickerStyle,
      noticeScreenDropdownItemPickerStyle,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={'rgba(1, 79, 112, 0.8)'}
                     barStyle="light-content"/>
        <View style={noticeScreenViewContainer}>
          <Dropdown
            label=""
            labelHeight={0}
            data={filterItems}
            value={this.state.selectedFilterItem
              ? this.state.selectedFilterItem
              : filterItems[0].value}
            itemCount={10}
            containerStyle={noticeScreenDropdownContainer}
            fontSize={20}
            baseColor={cssV('LIGHT_WHITE')}
            textColor={cssV('LIGHT_WHITE')}
            pickerStyle={noticeScreenDropdownPickerStyle}
            itemTextStyle={noticeScreenDropdownItemPickerStyle}
            selectedItemColor={cssV('CUSTOM_GRAY')}
            itemColor={cssV('EX_DARK_GRAY')}
            dropdownPosition={0}
            fontFamily={cssV('CUSTOM_TEXT_FONT')}
            paddingLeft={20}
            shadeOpacity={0}
            rippleOpacity={0}
            onChangeText={(value) => {
              this.props.isFetching = true;

              this.setState({
                selectedFilterItem: value,
              });

              const filteredNoticeData = noticesData.filter(function (obj) {
                return obj.type === value;
              });

              this.setState({
                renderNoticeData: filteredNoticeData,
              });
            }}
          />
          <Card
            style={noticeScreenListCard}>
            {this._renderNoticeList()}
          </Card>
        </View>
      </BackgroundImageContainer>
    );
  }
}

NoticesScreen.navigationOptions = ({navigation}) => ({
  title: 'Notices',
  headerTitleStyle: {
    color: '#fff',
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
  },
  headerStyle: {
    backgroundColor: '#026188',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_CUSTOM_HEADER_BACK}/>,
  headerRight: <BarButton navigation={navigation}/>,
  drawerIcon: ({tintColor}) => (
    <Icon name='bell-ring'
          size={cssV('DRAWER_ICON_SIZE')}
          style={{color: tintColor}}
    />
  ),
});

const mapStateToProps = ({notices}) => {
  const {noticesData, dataFetched, isFetching, error} = notices;
  return {noticesData, dataFetched, isFetching, error};
};

export default connect(mapStateToProps, {getNotices})(
  NoticesScreen);