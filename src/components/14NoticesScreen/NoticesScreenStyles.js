/**
 * Created by rajanmaharjan on 9/2/17.
 */

import {
  Platform,
  Dimensions,
} from 'react-native';

import cssV from '../../styles/variables/cssV';

const {height, width} = Dimensions.get('window');

const noticeScreenStyles = () => {
  return {
    noticeScreenContainer: {
      flexGrow: 1,
    },
    noticeScreenViewContainer: {
      // flex: 1,
      marginTop: Platform.OS === 'ios' ? 55 : 44,
      marginBottom: 70,
    },
    noticeScreenListCard: {
      backgroundColor: cssV('PRIMARY_WHITE'),
      marginTop: 12,
      marginLeft: 10,
      marginRight: 10,
      padding: 10,
      // paddingBottom: 0,
      marginBottom: 90,
    },
    noticeScreeListLoading: {
      margin: 10,
      padding: 7,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    noticeScreeListLoadingText: {
      paddingTop: 20,
      fontSize: 18,
      color: cssV('DARK_GRAY'),
    },
    noticeScreeListViewContainer: {
      flexDirection: 'row',
      padding: 5,
      // marginBottom: 10,
      borderBottomWidth: 0.5,
      borderBottomColor: cssV('LIGHT_GRAY'),
      // backgroundColor: 'red',
    },
    noticeScreenImageContainer: {
      alignItems: 'center',
      paddingTop: 10,
    },
    noticeScreenDropdownContainer: {
      backgroundColor: cssV('DARK_TEAL'),
      borderColor: cssV('DARK_TEAL'),
      borderWidth: 5,
      borderRadius: 5,
      marginLeft: 10,
      marginRight: 10,
      marginTop: Platform.OS === 'ios' ? 7 : 14,
      height: 40,
    },
    noticeScreenDropdownPickerStyle: {
      marginTop: 55,
      marginLeft: 5,
      marginRight: 5,
      width: width - 25,
    },
    noticeScreenDropdownItemPickerStyle: {
      fontSize: 18,
      fontWeight: '400',
      paddingLeft: 20,
      color: cssV('LIGHT_WHITE'),
    },
    noticeScreenImageStyle: {
      width: 26,
      height: 26,
      backgroundColor: 'transparent',
      borderRadius: 13,
      resizeMode: 'cover',
      marginRight: 10,
    },
    noticeScreenDataContainer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      paddingLeft: 10,
    },
    noticeScreenDataTitle: {
      fontSize: 18,
      fontWeight: '400',
      paddingBottom: 10,
      paddingTop: 5,
      color: cssV('DARK_GRAY'),
    },
    noticeScreenDataContent: {
      color: cssV('EX_DARK_GRAY'),
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      fontSize: 20,
      fontWeight: '400',
    },
    noticeScreenDataContentContainer: {
      marginBottom: 10,
    },
    noticeScreenTimeContainer: {
      justifyContent: 'center',
      alignSelf: 'flex-end',
    },
    noticeScreenTimeStyle: {
      color: cssV('LIGHT_GRAY'),
      fontSize: 10,
    },
  };
};

export default noticeScreenStyles;