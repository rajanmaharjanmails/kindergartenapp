/**
 * Created by rajanmaharjan on 7/11/17.
 */

import cssV from '../../styles/variables/cssV';

const StartScreenStyles = () => {
  return {
    startScreenContainer: {
      flexGrow: 1,
    },
    startScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    startScreenImgLogo: {
      width: 200,
      height: 200,
    },
    startScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 100,
    },
    startScreenLoadingScreen: {
      flex: 1,
      backgroundColor: cssV('LIGHT_BLUE'),
    },
    startScreenSpinnerTextStyle: {
      alignSelf: 'center',
      color: cssV('PRIMARY_WHITE'),
      fontSize: 16,
      fontWeight: '300',
      paddingTop: 10,
      fontFamily: 'Sniglet-Regular',
    },
  };
};

export default StartScreenStyles ;