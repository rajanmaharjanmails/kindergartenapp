/**
 * Created by rajanmaharjan on 8/19/17.
 */

import React, {
  Component,
} from 'react';

import {
  StyleSheet,
  Image,
  View,
} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import session from '../../utils/UserSession';

import {
  MyStatusBar,
  BackgroundImageContainer,
  CustomButton,
} from '../1common';

import {
  NAV_SINGIN_SCREEN,
  NAV_CREATE_ACC_SCREEN,
  NAV_ACTIVITIES_SCREEN,
} from '../../actions/types';

import {
  BUTTON_DEFAULT,
  BUTTON_TRANSPARENT,
} from '../1common/Button/ButtonTypes';

import cssV from '../../styles/variables/cssV';
import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class StartScreen extends Component {

  constructor () {
    super();
    this.state = {
      isLoaded: false,
    };

    this.session = new session.Session();
  }

  componentDidMount () {
    const {
      navigation,
    } = this.props;

    SplashScreen.hide();
    this.session.isLoggedIn().then((response) => {
      this.setState({
        isLoaded: true,
      });
      if (response)
        navigation.dispatch({type: NAV_ACTIVITIES_SCREEN});
    });
  }

  renderLoaderScreen = () => {
    const {startScreenLoadingScreen, startScreenSpinnerTextStyle} = styles;
    return (
      <View style={startScreenLoadingScreen}>
        <MyStatusBar backgroundColor={cssV('LIGHT_BLUE')}
                     barStyle="light-content"/>
        <Spinner
          visible={true}
          textContent={'Getting ready...'}
          textStyle={startScreenSpinnerTextStyle}
          overlayColor='rgba(0, 0, 0, 0)'
          animation="fade" //slide
        />
      </View>
    );
  };

  render () {

    const {
      navigation,
    } = this.props;
    const {
      startScreenContainer,
      startScreenImgLogo,
      startScreenCustomBtn,
      startScreenViewContainer,
    } = styles;

    if (!this.state.isLoaded)
      return (
        <View style={{flex: 1}}>
          {this.renderLoaderScreen()}
        </View>
      );

    return (
      <BackgroundImageContainer
        imageNumber={1}
      >
        <MyStatusBar backgroundColor={'rgba(0, 0, 0, 0.2)'}
                     barStyle="light-content"/>
        <KeyboardAwareScrollView contentContainerStyle={startScreenContainer}>
          <View style={startScreenViewContainer}>
            <Image
              source={require('../../assets/logo/Kindergarten-logoname.png')}
              style={startScreenImgLogo}
            />
            <CustomButton
              onPress={() => navigation.dispatch(
                {type: NAV_CREATE_ACC_SCREEN})}
              buttonType={BUTTON_DEFAULT}
              style={startScreenCustomBtn}
            >
              Create Account
            </CustomButton>
            <CustomButton
              onPress={() => navigation.dispatch({type: NAV_SINGIN_SCREEN})}
              buttonType={BUTTON_TRANSPARENT}
            >
              Sign In
            </CustomButton>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

StartScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: 'transparent',
    shadowOpacity: 0,
    position: 'absolute',
  },
};

export default StartScreen;