/**
 * Created by rajanmaharjan on 9/2/17.
 */
import {
  Platform,
} from 'react-native';

import cssV from '../../styles/variables/cssV';

const ActivitiesCalendarScreenStyles = () => {
  return {
    activitiesCalScreenContainer: {
      flexGrow: 1,
    },
    activitiesCalScreenViewContainer: {
      flex: 1,
      marginTop: Platform.OS === 'ios' ? 48 : 44,
      marginBottom: 55,
    },
    activitiesCalendarContainer: {
      backgroundColor: cssV('DARK_TEAL'),
      padding: 10,
      paddingBottom: 20,
      paddingTop: 10,
      marginBottom: 0,
    },
    activitiesListContainer: {
      flex: 1,
    },
    activitiesCalCardStyle: {
      backgroundColor: cssV('PRIMARY_WHITE'),
      marginTop: 15,
      marginBottom: 10,
      paddingBottom: 5,
      marginLeft: 20,
      marginRight: 20,
    },
    activitiesCalCardListStyle: {
      margin: 10,
      marginTop: 0,
      marginBottom: 0,
      padding: 7,
      borderBottomColor: '#959595',
      borderBottomWidth: 0.5,
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    activitiesCalCardListLoading: {
      margin: 10,
      marginTop: 40,
      marginBottom: 0,
      padding: 7,
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    activitiesCalCardListLoadingText: {
      fontSize: 18,
      color: cssV('DARK_GRAY'),
    },
    activitiesCalSummaryCard: {
      backgroundColor: cssV('PRIMARY_WHITE'),
      marginTop: 12,
      marginLeft: 20,
      marginRight: 20,
      padding: 10,
    },
    activitiesCalTitleStyle: {
      color: cssV('EX_DARK_GRAY'),
      fontSize: 16,
    },
    activitiesCalCardListDateContainer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    activitiesCalCardListDate: {
      color: '#342c33',
      fontSize: 18
    },
    activitiesCalCardListMonth: {
      color: '#342c33',
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      fontSize: 16,
    },
    activitiesCalCardListDescription: {
      flex: 5,
      flexDirection: 'column',
      justifyContent: 'center',
    },
    activitiesCalCardListDescriptionTitle: {
      color: '#342c33',
      fontSize: 18
    },
    activitiesCalCardListDescriptionInfo: {
      color: '#342c33',
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      fontSize: 18,
    },
    activitiesCalImageContainer: {
      flex: 1
    },
    activitiesCalImageStyle: {
      flex: 1,
      width: 36,
      height: 36,
      backgroundColor: 'transparent',
      borderRadius: 18,
      resizeMode: 'cover',
      justifyContent: 'center',
      alignItems: 'center',
      margin: 10,
    },
  };
};

export default ActivitiesCalendarScreenStyles;