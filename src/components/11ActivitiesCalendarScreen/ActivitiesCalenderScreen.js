/**
 * Created by rajanmaharjan on 9/2/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  ScrollView,
  StyleSheet,
  Platform,
  View,
  Text,
  Image,
  RefreshControl,
  FlatList,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Calendar } from 'react-native-calendars';
import MyText from 'react-native-letter-spacing';
import moment from 'moment';
import Moment from 'moment-weekdaysin';

import {
  MyStatusBar,
  BackgroundImageContainer,
  BarButton,
  DefaultBackButton,
  CardSection,
  Card,
  ProfileImageComponent,
} from '../1common';

import {
  BUTTON_CUSTOM_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import {
  getCalActivities,
} from '../../actions';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

const calendarTheme = {
  calendarBackground: cssV('DARK_TEAL'),
  textSectionTitleColor: cssV('LIGHT_WHITE'),
  selectedDayBackgroundColor: 'red',
  selectedDayTextColor: cssV('LIGHT_TEAL'),
  todayTextColor: '#11a2ef',
  dayTextColor: cssV('LIGHT_WHITE'),
  textDisabledColor: cssV('LIGHT_GRAY'),
  dotColor: '#ffffff',
  selectedDotColor: 'red',
  arrowColor: cssV('LIGHT_WHITE'),
  monthTextColor: cssV('LIGHT_WHITE'),
  textDayFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textMonthFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textDayHeaderFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textDayFontSize: 16,
  textMonthFontSize: 20,
  textDayHeaderFontSize: 16,
};

class ActivitiesCalendarScreen extends Component {

  constructor() {
    super();

    this.state = {
      currentDate: moment().format('YYYY-MM-DD'),
      currentMonth: moment().month() + 1,
    };

    this._getCalendarActivities = this._getCalendarActivities.bind(this);
    this._renderActivitiesDetails = this._renderActivitiesDetails.bind(this);
    this._renderCalendar = this._renderCalendar.bind(this);
    this._getMarkedDays = this._getMarkedDays.bind(this);
  }

  _getCalendarActivities() {
    this.props.getCalActivities();
  }

  componentDidMount() {
    this._getCalendarActivities();
  }

  renderArrow = (direction) => {
    let iconName = '';
    if (direction === 'left')
      iconName = 'chevron-left';
    else
      iconName = 'chevron-right';

    return (
      <Icon name={iconName}
        size={25}
        style={{
          color: cssV('LIGHT_WHITE'),
        }}
      />);
  };

  _keyExtractor = (item, index) => item.id;

  _renderActivities = ({ item }) => {
    const activities = item;
    // console.warn(item);

    const {
      activitiesCalCardStyle,
      activitiesCalCardListStyle,
      activitiesCalCardListDateContainer,
      activitiesCalCardListDate,
      activitiesCalCardListMonth,
      activitiesCalCardListDescription,
      activitiesCalCardListDescriptionTitle,
      activitiesCalCardListDescriptionInfo,
      activitiesCalImageContainer,
      activitiesCalImageStyle,
    } = styles;

    const activitiesImage = (activities.photo_url && photo_url.thumb)
      ? (
        <Image
          source={{ uri: activities.photo_url.thumb }}
          style={activitiesCalImageStyle}
        />
      ) : (null);

    return (
      <Card style={activitiesCalCardStyle}>
        <CardSection style={activitiesCalCardListStyle}>
          <CardSection style={activitiesCalCardListDateContainer}>
            <Text style={activitiesCalCardListDate}>
              {moment(activities.date).format('D')}
            </Text>
            <MyText
              letterSpacing={1}
              wordSpacing={5}
              style={activitiesCalCardListMonth}
            >
              {moment(activities.date).format('MMM')}
            </MyText>
          </CardSection>
          <CardSection style={activitiesCalCardListDescription}>
            <Text style={activitiesCalCardListDescriptionTitle}>
              {activities.title}
            </Text>
            <MyText
              letterSpacing={1}
              wordSpacing={5}
              style={activitiesCalCardListDescriptionInfo}
            >
              {activities.description}
            </MyText>
          </CardSection>
          <ProfileImageComponent style={activitiesCalImageContainer}>
            {activitiesImage}
          </ProfileImageComponent>
        </CardSection>
      </Card>
    );
  };

  _renderActivitiesDetails = () => {

    const { calActivitiesData, isFetching } = this.props;

    const {
      activitiesCalCardListLoading,
      activitiesCalCardListLoadingText,
      activitiesCalSummaryCard,
      activitiesCalTitleStyle
    } = styles;

    if (isFetching)
      return (
        <CardSection
          style={activitiesCalCardListLoading}>
          <Text style={activitiesCalCardListLoadingText}>
            Updating activities...
          </Text>
        </CardSection>
      );

    let currentMonthActivities = calActivitiesData.map((item) => {
      let activitiesMonth = moment(item.date).month() + 1;
      if (activitiesMonth === this.state.currentMonth)
        return item;
      else
        return null;
    }).filter(item => item !== null);

    if (currentMonthActivities.length)
      return (
        <FlatList
          data={currentMonthActivities}
          extraData={this.state.currentMonth}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderActivities}
        />
      );

    return (
      <Card
        style={activitiesCalSummaryCard}>
        <Text style={activitiesCalTitleStyle}>
          No activities this month.
        </Text>
      </Card>
    );
  };

  _getMarkedDays = () => {

    const { calActivitiesData, isFetching } = this.props;

    if (isFetching)
      return null;

    let markedDays = {};
    let SaturdayStyle = [{ textColor: '#edc200' }];
    let ActivitiesStyle = [
      { startingDay: true, color: '#00adf5', },
      { endingDay: true, color: '#00adf5' }];

    const Saturdays = Moment().weekdaysInYear('Saturday');
    // const NextYerSaturdays = Moment(moment().add(1, 'y')).
    //   weekdaysInYear('Saturday');

    Saturdays.map((item) => {
      markedDays[moment(item).format('YYYY-MM-DD')] = SaturdayStyle;
    });

    // NextYerSaturdays.map((item) => {
    //   markedDays[moment(item).format('YYYY-MM-DD')] = SaturdayStyle;
    // });

    calActivitiesData.map((item) => {
      markedDays[moment(item.date).format('YYYY-MM-DD')] = ActivitiesStyle;
    });

    return markedDays;
  };

  _renderCalendar = () => {
    return (
      <Calendar
        current={this.state.currentDate}
        minDate={moment().subtract(6, 'M').format('YYYY-MM-DD')}
        maxDate={moment().add(1, 'y').format('YYYY-MM-DD')}
        // onDayPress={(day) => {console.warn('selected day', day);}}
        monthFormat={'MMMM yyyy'}
        onMonthChange={(selectedDate) => {
          this.setState({
            currentMonth: selectedDate.month,
            currentDate: moment(selectedDate.dateString).format('YYYY-MM-DD'),
          });
        }}
        hideArrows={false}
        renderArrow={(direction) => (this.renderArrow(direction))}
        hideExtraDays={false}
        disableMonthChange={false}
        firstDay={0}
        markingType={'interactive'}
        markedDates={this._getMarkedDays()}
        theme={calendarTheme}
      />
    );
  };

  render() {
    const { isFetching } = this.props;

    const {
      activitiesCalScreenContainer,
      activitiesCalScreenViewContainer,
      activitiesCalendarContainer,
      activitiesListContainer,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={'rgba(1, 79, 112, 0.8)'}
          barStyle="light-content" />
        <ScrollView
          contentContainerStyle={activitiesCalScreenContainer}
          style={activitiesCalScreenViewContainer}
          refreshControl={
            <RefreshControl
              refreshing={isFetching}
              onRefresh={this._getCalendarActivities}
            />
          }
        >
          <View style={activitiesCalendarContainer}>
            {this._renderCalendar()}
          </View>
          <View style={activitiesListContainer}>
            {this._renderActivitiesDetails()}
          </View>
        </ScrollView>
      </BackgroundImageContainer>
    );
  }
}

ActivitiesCalendarScreen.navigationOptions = ({ navigation }) => ({
  title: 'Activities',
  headerTitleStyle: {
    color: cssV('PRIMARY_WHITE'),
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
  },
  headerStyle: {
    backgroundColor: cssV('DARK_TEAL'),
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
    buttonType={BUTTON_CUSTOM_HEADER_BACK} />,
  headerRight: <BarButton navigation={navigation} />,
  drawerLabel: 'Calendar',
  drawerIcon: ({ tintColor }) => (
    <Icon name='calendar-text'
      size={cssV('DRAWER_ICON_SIZE')}
      style={{ color: tintColor }}
    />
  ),
});

const mapStateToProps = ({ calendarActivities }) => {
  const { calActivitiesData, dataFetched, isFetching, error } = calendarActivities;
  return { calActivitiesData, dataFetched, isFetching, error };
};

export default connect(mapStateToProps, { getCalActivities })(
  ActivitiesCalendarScreen);