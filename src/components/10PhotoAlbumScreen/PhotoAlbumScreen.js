/**
 * Created by rajanmaharjan on 8/31/17.
 */

import React, {
  Component,
} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Platform,
} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
  MyStatusBar,
  BackgroundImageContainer,
  CustomButton,
  BarButton,
  DefaultBackButton,
} from '../1common';

import {
  NAV_PHOTO_GALLERY_SCREEN,
} from '../../actions/types';

import {
  BUTTON_DEFAULT,
  BUTTON_CUSTOM_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class PhotoAlbumScreen extends Component {

  componentDidMount () {
    SplashScreen.hide();
  }

  render () {
    const {
      navigation,
    } = this.props;
    const {
      photoAlbumScreenContainer,
      photoAlbumScreenCustomBtn,
      photoAlbumScreenViewContainer,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={'rgba(1, 79, 112, 0.9)'}
                     barStyle="light-content"/>
        <KeyboardAwareScrollView
          contentContainerStyle={photoAlbumScreenContainer}>
          <View style={photoAlbumScreenViewContainer}>
            <Text>
              PhotoAlbum
            </Text>
            <CustomButton
              onPress={() => navigation.dispatch(
                {type: NAV_PHOTO_GALLERY_SCREEN})}
              buttonType={BUTTON_DEFAULT}
              style={photoAlbumScreenCustomBtn}
            >
              Photo Gallery
            </CustomButton>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

PhotoAlbumScreen.navigationOptions = ({navigation}) => ({
  title: 'Photo Album',
  headerTitleStyle: {
    color: '#fff',
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
    // paddingLeft: (Platform.OS === 'ios' ? 0 : 40),
  },
  headerStyle: {
    backgroundColor: '#026188',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_CUSTOM_HEADER_BACK}/>,
  headerRight: <BarButton navigation={navigation}/>,
  drawerLabel: 'Gallery',
  drawerIcon: ({tintColor}) => (
    <Icon name='image-album'
          size={cssV('DRAWER_ICON_SIZE')}
          style={{color: tintColor}}
    />
  ),
});

export default PhotoAlbumScreen;
