/**
 * Created by rajanmaharjan on 8/31/17.
 */
import cssV from '../../styles/variables/cssV';

const PhotoAlbumScreenStyles = () => {
  return {
    photoAlbumScreenContainer: {
      flexGrow: 1,
    },
    photoAlbumScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    photoAlbumScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 100,
      backgroundColor: cssV('DARK_TEAL'),
    },
  };
};

export default PhotoAlbumScreenStyles ;