/**
 * Created by rajanmaharjan on 9/10/17.
 */
import cssV from '../../styles/variables/cssV';

const PhotoGalleryScreenStyles = () => {
  return {
    photoGalleryScreenContainer: {
      flexGrow: 1,
    },
    photoGalleryScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    photoGalleryScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 100,
      backgroundColor: cssV('DARK_TEAL'),
    },
  };
};

export default PhotoGalleryScreenStyles ;