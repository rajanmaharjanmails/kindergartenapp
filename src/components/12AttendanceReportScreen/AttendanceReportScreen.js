/**
 * Created by rajanmaharjan on 9/2/17.
 */
import React, {
  Component,
} from 'react';
import { connect } from 'react-redux';

import {
  StyleSheet,
  Platform,
  View,
  Text,
  ScrollView,
  RefreshControl,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Calendar } from 'react-native-calendars';
import MyText from 'react-native-letter-spacing';
import moment from 'moment';
import Moment from 'moment-weekdaysin';

import {
  MyStatusBar,
  BackgroundImageContainer,
  BarButton,
  DefaultBackButton,
  CardSection,
} from '../1common';

import {
  BUTTON_CUSTOM_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import {
  getAttendanceReport,
} from '../../actions';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

const calendarTheme = {
  calendarBackground: cssV('DARK_TEAL'),
  textSectionTitleColor: cssV('LIGHT_WHITE'),
  selectedDayBackgroundColor: 'red',
  selectedDayTextColor: cssV('LIGHT_TEAL'),
  todayTextColor: '#00adf5',
  dayTextColor: cssV('LIGHT_WHITE'),
  textDisabledColor: cssV('LIGHT_GRAY'),
  dotColor: '#00adf5',
  selectedDotColor: 'red',
  arrowColor: cssV('LIGHT_WHITE'),
  monthTextColor: cssV('LIGHT_WHITE'),
  textDayFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textMonthFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textDayHeaderFontFamily: cssV('GLOBAL_TEXT_FONT'),
  textDayFontSize: 16,
  textMonthFontSize: 20,
  textDayHeaderFontSize: 16,
};

class AttendanceReportScreen extends Component {

  constructor() {
    super();

    this.state = {
      currentDate: moment().format('YYYY-MM-DD'),
      currentMonth: moment().month() + 1,
      currentYear: moment().year(),
    };

    this._getAttendanceReport = this._getAttendanceReport.bind(this);
    this._getMarkedDays = this._getMarkedDays.bind(this);
    this._renderCalendar = this._renderCalendar.bind(this);
    this._renderAttendanceDetails = this._renderAttendanceDetails.bind(this);
  }

  _getAttendanceReport(month, year) {
    let currentMonth = month ? month : this.state.currentMonth;
    let currentYear = year ? year : this.state.currentYear;
    this.props.getAttendanceReport(currentYear, currentMonth);
  }

  componentDidMount() {
    this._getAttendanceReport();
  }

  renderArrow = (direction) => {
    let iconName = '';
    if (direction === 'left')
      iconName = 'chevron-left';
    else
      iconName = 'chevron-right';

    return (
      <Icon name={iconName}
        size={25}
        style={{
          color: cssV('LIGHT_WHITE'),
        }}
      />);
  };

  _getAttendanceInfoContainer = (days, title, color) => {
    const {
      attCalCardDetailContainerOuter,
      attCalCardDetailContainerInner,
      attCalCardDetailDaysText,
      attCalCardDetailTitleText,
    } = styles;

    // As design
    return (
      <View style={[attCalCardDetailContainerOuter, { borderColor: color }]}>
        <View style={[attCalCardDetailContainerInner, { borderColor: color }]}>
          <Text style={attCalCardDetailDaysText}>
            {days}
          </Text>
          <MyText
            letterSpacing={1}
            wordSpacing={5}
            style={attCalCardDetailTitleText}>
            {title}
          </MyText>
        </View>
      </View>
    );

    // return (
    //   <View style={{justifyContent: 'center', alignItems: 'center'}}>
    //     <View style={[attCalCardDetailContainerOuter, {borderColor: color}]}>
    //       <View style={[attCalCardDetailContainerInner, {borderColor: color}]}>
    //         <Text style={attCalCardDetailDaysText}>
    //           {days}
    //         </Text>
    //       </View>
    //     </View>
    //     <MyText
    //       letterSpacing={1}
    //       wordSpacing={5}
    //       style={attCalCardDetailTitleText}>
    //       {title}
    //     </MyText>
    //   </View>
    // );
  };

  _renderAttendanceDetails = () => {

    let {
      attendanceReportData, isFetching,
    } = this.props;

    const {
      attendanceCalCardStyle, attendanceCalCardContainer,
      attCalCardListLoading, attCalCardListLoadingText,
    } = styles;


    let totalDays = (attendanceReportData.days)
      ? attendanceReportData.days.total : 0;
    let presentDays = attendanceReportData.days
      ? attendanceReportData.days.present : 0;
    let absentDays = attendanceReportData.days
      ? attendanceReportData.days.absent : 0;
    let lateDays = attendanceReportData.days
      ? attendanceReportData.days.late : 0;

    if (isFetching)
      return (
        <CardSection
          style={attCalCardListLoading}>
          <Text style={attCalCardListLoadingText}>
            Updating details...
          </Text>
        </CardSection>
      );

    return (
      <CardSection
        style={attendanceCalCardStyle}>
        <View style={attendanceCalCardContainer}>
          {this._getAttendanceInfoContainer(totalDays, 'Total Days',
            cssV('LIGHT_TEAL'))}
          {this._getAttendanceInfoContainer(presentDays, 'Present',
            cssV('GREEN'))}
        </View>
        <View style={attendanceCalCardContainer}>
          {this._getAttendanceInfoContainer(absentDays, 'Absent', cssV('RED'))}
          {this._getAttendanceInfoContainer(lateDays, 'Late', cssV('ORANGE'))}
        </View>
      </CardSection>
    );
  };

  _getMarkedDays = () => {

    let {
      attendanceReportData, isFetching,
    } = this.props;

    if (isFetching)
      return null;

    let attendanceDetails = attendanceReportData.attendance_details
      ? attendanceReportData.attendance_details : {};

    let markedDays = {};
    let SaturdayStyle = [{ textColor: cssV('ORANGE') }];
    let AbsentStyle = [
      { startingDay: true, color: cssV('RED'), },
      { endingDay: true, color: cssV('RED') }];
    let LateStyle = [
      { startingDay: true, color: cssV('ORANGE') },
      { endingDay: true, color: cssV('ORANGE') }];

    const Saturdays = Moment().weekdaysInYear('Saturday');
    // const NextYerSaturdays = Moment(moment().add(1, 'y')).
    //   weekdaysInYear('Saturday');

    Saturdays.map((item) => {
      markedDays[moment(item).format('YYYY-MM-DD')] = SaturdayStyle;
    });

    // NextYerSaturdays.map((item) => {
    //   markedDays[moment(item).format('YYYY-MM-DD')] = SaturdayStyle;
    // });

    Object.keys(attendanceDetails).forEach(function(key) {

      if (attendanceDetails[key] === 'A') {
        let dateKey = new Date(attendanceReportData.year,
          attendanceReportData.month - 1, key);
        markedDays[moment(dateKey).format('YYYY-MM-DD')] = AbsentStyle;
      }

      if (attendanceDetails[key] === 'L') {
        let dateKey = new Date(attendanceReportData.year,
          attendanceReportData.month - 1, key);
        markedDays[moment(dateKey).format('YYYY-MM-DD')] = LateStyle;
      }

    });

    return markedDays;
  };

  _renderCalendar = () => {
    return (
      <Calendar
        current={this.state.currentDate}
        minDate={moment().subtract(6, 'M').format('YYYY-MM-DD')}
        maxDate={moment().add(1, 'y').format('YYYY-MM-DD')}
        // onDayPress={(day) => {console.warn('selected day', day);}}
        monthFormat={'MMMM yyyy'}
        onMonthChange={(selectedDate) => {
          this.setState({
            currentMonth: selectedDate.month,
            currentYear: selectedDate.year,
            currentDate: moment(selectedDate.dateString).format('YYYY-MM-DD'),
          });
          this._getAttendanceReport(selectedDate.month, selectedDate.year);
        }}
        hideArrows={false}
        renderArrow={(direction) => (this.renderArrow(direction))}
        hideExtraDays={false}
        disableMonthChange={false}
        firstDay={0}
        markingType={'interactive'}
        markedDates={this._getMarkedDays()}
        theme={calendarTheme}
      />
    );
  };

  render() {
    const { isFetching } = this.props;

    const {
      attendanceCalScreenContainer,
      attendanceCalScreenViewContainer,
      attendanceCalContainer,
      attendanceDetailContainer,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={'rgba(1, 79, 112, 0.8)'}
          barStyle="light-content" />
        <ScrollView
          contentContainerStyle={attendanceCalScreenContainer}
          style={attendanceCalScreenViewContainer}
          refreshControl={
            <RefreshControl
              refreshing={isFetching}
              onRefresh={this._getAttendanceReport}
            />
          }
        >
          <View style={attendanceCalContainer}>
            {this._renderCalendar()}
          </View>
          <View style={attendanceDetailContainer}>
            {this._renderAttendanceDetails()}
          </View>
        </ScrollView>
      </BackgroundImageContainer>
    );
  }
}

AttendanceReportScreen.navigationOptions = ({ navigation }) => ({
  title: 'Attendance',
  headerTitleStyle: {
    color: '#fff',
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
  },
  headerStyle: {
    backgroundColor: '#026188',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
    buttonType={BUTTON_CUSTOM_HEADER_BACK} />,
  headerRight: <BarButton navigation={navigation} />,
  drawerIcon: ({ tintColor }) => (
    <Icon name='human-handsup'
      size={cssV('DRAWER_ICON_SIZE')}
      style={{ color: tintColor }}
    />
  ),
});

const mapStateToProps = ({ attendanceReports }) => {
  const { attendanceReportData, dataFetched, isFetching, error } = attendanceReports;
  return { attendanceReportData, dataFetched, isFetching, error };
};

export default connect(mapStateToProps, { getAttendanceReport })(
  AttendanceReportScreen);