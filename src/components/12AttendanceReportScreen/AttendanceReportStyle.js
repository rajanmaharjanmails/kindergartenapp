/**
 * Created by rajanmaharjan on 9/2/17.
 */

import {
  Platform,
} from 'react-native';

import cssV from '../../styles/variables/cssV';

const AttendanceCalScreenStyles = () => {
  return {
    attendanceCalScreenContainer: {
      flexGrow: 1,
    },
    attendanceCalScreenViewContainer: {
      flex: 1,
      marginTop: Platform.OS === 'ios' ? 48 : 44,
      // marginBottom: 55,
    },
    attendanceCalContainer: {
      backgroundColor: cssV('DARK_TEAL'),
      padding: 10,
      paddingBottom: 20,
      paddingTop: 10,
      marginBottom: 0,
    },
    attendanceDetailContainer: {
      flex: 1,
    },
    attendanceCalCardStyle: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: 'transparent',
      marginTop: 15,
      marginBottom: 55,
      marginLeft: 35,
      marginRight: 35,
    },
    attendanceCalCardContainer: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginBottom: 10,
    },
    attCalCardDetailContainerOuter: {
      width: 100,
      height: 100,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 100,
      borderWidth: 5,
    },
    attCalCardDetailContainerInner: {
      width: 80,
      height: 80,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 50,
      borderWidth: 2,
    },
    attCalCardDetailTitleText: {
      color: cssV('LIGHT_BLACK'),
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      fontSize: 18,
      fontWeight: '400',
    },
    attCalCardDetailDaysText: {
      fontSize: 23,
      color: cssV('EX_DARK_GRAY'),
    },
    attCalCardListLoading: {
      margin: 10,
      marginTop: 40,
      marginBottom: 0,
      padding: 7,
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    attCalCardListLoadingText: {
      fontSize: 18,
      color: cssV('DARK_GRAY'),
    },
    // attCalCardDetailTitleText: {
    //   color: cssV('LIGHT_BLACK'),
    //   fontFamily: cssV('CUSTOM_TEXT_FONT'),
    //   fontSize: 18,
    //   fontWeight: '400',
    //   marginTop: 10,
    // },
    // attCalCardDetailContainerOuter: {
    //   width: 55,
    //   height: 55,
    //   justifyContent: 'center',
    //   alignItems: 'center',
    //   borderRadius: 100,
    //   borderWidth: 3,
    // },
    // attCalCardDetailContainerInner: {
    //   width: 45,
    //   height: 45,
    //   justifyContent: 'center',
    //   alignItems: 'center',
    //   borderRadius: 50,
    //   borderWidth: 1,
    // },
  };
};

export default AttendanceCalScreenStyles;