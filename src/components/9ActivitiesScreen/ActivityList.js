/**
 * Created by rajanmaharjan on 8/7/17.
 */

'use strict';

import React, { Component } from 'react';
import ImageCapInset from 'react-native-image-capinsets';

import {
  StyleSheet,
  ListView,
  ImageBackground,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

let ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
  sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
});

let defaultCircleSize = 20;
let defaultCircleColor = '#007AFF';
let defaultLineWidth = 2;
let defaultLineColor = '#007AFF';

const window = Dimensions.get('window');

export default class ActivityList extends Component {
  constructor (props, context) {
    super(props, context);

    this._renderRow = this._renderRow.bind(this);

    this.renderDetail = (this.props.renderDetail
      ? this.props.renderDetail
      : this._renderDetail).bind(this);

    this.renderCircle = (this.props.renderCircle
      ? this.props.renderCircle
      : this._renderCircle).bind(this);

    this.renderEvent = this._renderEvent.bind(this);

    this.onEventPress = this.props.onEventPress;

    this.state = {
      data: this.props.data,
      dataSource: ds.cloneWithRows(this.props.data),
      x: 0,
    };
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      data: nextProps.data,
      dataSource: ds.cloneWithRows(nextProps.data),
    });
  }

  render () {
    return (
      <View style={[styles.container, this.props.style]}>
        <ListView
          ref='listView'
          style={[styles.listview]}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow}
          automaticallyAdjustContentInsets={false}
          {...this.props.options}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }

  _renderRow (rowData, sectionID, rowID) {
    return (
      <View key={rowID}>
        <View style={[styles.rowContainer, this.props.rowContainerStyle]}>
          {this.renderEvent(rowData, sectionID, rowID)}
          {this.renderCircle(rowData, sectionID, rowID)}
        </View>
      </View>
    );
  }

  _renderEvent (rowData, sectionID, rowID) {
    let lineWidth = rowData.lineWidth
      ? rowData.lineWidth
      : this.props.lineWidth;
    let isLast = this.state.data.slice(-1)[0] === rowData;
    let lineColor = isLast ? ('rgba(0,0,0,0)') : (rowData.lineColor
      ? rowData.lineColor
      : this.props.lineColor);

    return (

      <View style={[
        styles.details, {
          borderColor: lineColor,
          borderLeftWidth: lineWidth,
          borderRightWidth: 0,
          marginLeft: 20,
          paddingLeft: 15,
          paddingTop: 15,
          marginBottom: -15,
        }]}
            onLayout={(evt) => {
              if (!this.state.x && !this.state.width) {
                var {x, width} = evt.nativeEvent.layout;
                this.setState({x, width});
              }
            }}>
        <TouchableOpacity disabled={this.props.onEventPress === null}
                          style={[
                            {marginLeft: 1},
                            this.props.detailContainerStyle]}
                          onPress={() => this.props.onEventPress
                            ? this.props.onEventPress(rowData)
                            : null}
        >
          <View style={{paddingTop: 0}}>
            {this.renderDetail(rowData, sectionID, rowID)}
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  _renderDetail (rowData, sectionID, rowID) {

    let title = <Text
      style={[styles.title, this.props.titleStyle]}>{rowData.title}</Text>;
    if (rowData.description)
      title = (
        <ImageCapInset
          source={require('../../assets/images/message_bubble_left.png')}
          capInsets={{top: 30, left: 13, bottom: 18, right: 13}}
          resizeMode='stretch'
          style={{justifyContent: 'center', width: null, height: null}}
        >
          <View
            style={{padding: 20}}>
            <Text
              style={[
                styles.title,
                this.props.titleStyle]}>{rowData.title}</Text>
            <Text style={[
              styles.description,
              this.props.descriptionStyle]}>{rowData.description}</Text>
            <Text
              style={[
                styles.description,
                this.props.descriptionStyle,
                {paddingBottom: 10}]}>{rowData.time}</Text>
          </View>
        </ImageCapInset>
      );
    return (
      <View style={styles.container}>
        {title}
      </View>

    );
  }

  _renderCircle (rowData, sectionID, rowID) {
    let circleSize = rowData.circleSize
      ? rowData.circleSize
      : this.props.circleSize
        ? this.props.circleSize
        : defaultCircleSize;
    let circleColor = rowData.circleColor
      ? rowData.circleColor
      : this.props.circleColor
        ? this.props.circleColor
        : defaultCircleColor;
    let lineWidth = rowData.lineWidth
      ? rowData.lineWidth
      : this.props.lineWidth
        ? this.props.lineWidth
        : defaultLineWidth;

    let iconSource = rowData.icon ? rowData.icon : this.props.icon;
    let iconStyle = {
      height: circleSize - 10,
      width: circleSize - 10,
    };
    let innerCircle = (
      <ImageBackground source={iconSource} style={[iconStyle, this.props.iconStyle]}/>);

    return (
      <View style={[
        styles.circle, {
          width: this.state.x ? circleSize : 0,
          height: this.state.x ? circleSize : 0,
          borderRadius: circleSize / 2,
          borderColor: circleColor,
          borderWidth: this.props.lineWidth ? this.props.lineWidth : 3,
          left: this.state.x - (circleSize / 2) + ((lineWidth - 1) / 2),
          marginTop: 20,
          backgroundColor: '#0a91dd',
        }, this.props.circleStyle]}>
        {innerCircle}
      </View>
    );
  }
}

ActivityList.defaultProps = {
  circleSize: defaultCircleSize,
  circleColor: defaultCircleColor,
  lineWidth: defaultLineWidth,
  lineColor: defaultLineColor,
};

let styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listview: {
    flex: 1,
  },
  sectionHeader: {
    marginBottom: 15,
    backgroundColor: '#007AFF',
    height: 30,
    justifyContent: 'center',
  },
  sectionHeaderText: {
    color: '#FFF',
    fontSize: 18,
    alignSelf: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  circle: {
    width: 30,
    height: 30,
    borderRadius: 10,
    position: 'absolute',
    left: -8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    fontWeight: '400',
    color: 'gray',
  },
  details: {
    borderLeftWidth: defaultLineWidth,
    flexDirection: 'column',
    flex: 1,
  },
  description: {
    marginTop: 10,
  },
});
