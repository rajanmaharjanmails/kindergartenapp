/**
 * Created by rajanmaharjan on 8/19/17.
 */

import React, {
  Component,
} from 'react';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import RNExitApp from 'react-native-exit-app';

import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  RefreshControl,
  ActivityIndicator,
  BackHandler,
  Alert,
} from 'react-native';

import {
  NAV_ACTIVITIES_DETAIL_SCREEN,
} from '../../actions/types';

import {
  MyStatusBar,
  BackgroundImageContainer,
  HeaderComponent,
  ProfileImageComponent,
  CardSection,
  BarButton,
} from '../1common';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ActivityList from './ActivityList';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import SplashScreen from 'react-native-splash-screen';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class ActivitiesScreen extends Component {
  constructor () {
    super();

    this.onEndReached = this.onEndReached.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onRefresh = this.onRefresh.bind(this);

    this.data = [
      {
        time: '09:00',
        title: 'Archery Training',
        description: 'Event 1 Description',
        icon: require('../../assets/icons/archery.png'),
      },
      {
        time: '10:45',
        title: 'Event 2',
        description: 'Event 2 Description',
        icon: require('../../assets/icons/badminton.png'),
      },
      {
        time: '12:00',
        title: 'Event 3',
        description: 'Event 3 DescriptionEvent 3 DescriptionEvent 3 DescriptionEvent 3 Description',
        icon: require('../../assets/icons/lunch.png'),
      },
      {
        time: '14:00',
        title: 'Event 4',
        description: 'Event 4 Description',
        icon: require('../../assets/icons/soccer.png'),
      },
      {
        time: '16:30',
        title: 'Event 5',
        description: 'Event 5 Description',
        icon: require('../../assets/icons/dumbbell.png'),
      },
    ];

    this.state = {
      isRefreshing: false,
      waiting: false,
      data: this.data,
      dropdownSelection: 'All Activities',
    };
  }

  componentDidMount () {
    SplashScreen.hide();
    BackHandler.addEventListener('hardwareBackPress',
      this.onBackPress);
  }

  componentWillUnmount () {
    BackHandler.removeEventListener('hardwareBackPress',
      this.onBackPress);
  }

  onBackPress = () => {
    const {dispatch} = this.props;
    Alert.alert('Exit Application?', 'Are you sure you want to exit?', [
      {
        text: 'Cancel',
        onPress: () => true,
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => RNExitApp.exitApp(),
      }]);
    dispatch(NavigationActions.back());
    return true;

  };

  /*Activity timeline components*/
  onRefresh () {
    this.setState({isRefreshing: true});
    //refresh to initial data
    setTimeout(() => {
      //refresh to initial data
      this.setState({
        data: this.data,
        isRefreshing: false,
      });
    }, 2000);
  }

  onEndReached () {
    if (!this.state.waiting) {
      this.setState({waiting: true});

      //fetch and concat data
      setTimeout(() => {

        //refresh to initial data
        var data = this.state.data.concat(
          {
            time: '09:00',
            title: 'Archery Training',
            description: 'Event 1 Description',
            icon: require('../../assets/icons/archery.png'),
          },
          {
            time: '10:45',
            title: 'Event 2',
            description: 'Event 2 Description',
            icon: require('../../assets/icons/badminton.png'),
          },
          {
            time: '12:00',
            title: 'Event 3',
            description: 'Event 3 Description',
            icon: require('../../assets/icons/lunch.png'),
          },
          {
            time: '14:00',
            title: 'Event 4',
            description: 'Event 4 Description',
            icon: require('../../assets/icons/soccer.png'),
          },
          {
            time: '16:30',
            title: 'Event 5',
            description: 'Event 5 Description',
            icon: require('../../assets/icons/dumbbell.png'),
          },
        );

        this.setState({
          waiting: false,
          data: data,
        });
      }, 2000);
    }
  }

  renderFooter () {
    if (this.state.waiting) {
      return <ActivityIndicator />;
    } else {
      return <Text>~</Text>;
    }
  }

  /*Activity timeline components*/

  render () {
    const {navigation} = this.props;

    const {
      activitiesScreenProfileImageComponent,
      activitiesScreenProfileImageBorder,
      activitiesScreenProfileImageContainer,
      activitiesScreenProfileImageStyle,
      activitiesHeaderTextStyle,
      activitiesHeaderCardStyle,
    } = styles;

    const bgContainerProps = {
      imageNumber: 3,
      imageContainer: {
        opacity: 0.3,
      },
    };

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={'rgba(1, 79, 112, 0.9)'}
                     barStyle="light-content"/>
        <HeaderComponent>
          <BackgroundImageContainer {...bgContainerProps}>
            <ProfileImageComponent
              style={activitiesScreenProfileImageComponent }>
              <View style={activitiesScreenProfileImageBorder}>
                <ImageBackground
                  source={require('../../assets/images/child.png')}
                  style={activitiesScreenProfileImageContainer}
                  imageStyle={activitiesScreenProfileImageStyle}
                />
              </View>
              <Text
                style={activitiesHeaderTextStyle}>
                John Snow
              </Text>
            </ProfileImageComponent>
          </BackgroundImageContainer>
        </HeaderComponent>
        <CardSection style={activitiesHeaderCardStyle}/>
        <CardSection style={{
          flex: 1,
          marginLeft: 20,
          marginRight: 20,
          marginBottom: 55,
          backgroundColor: 'transparent',
          shadowOpacity: 0,
        }}>
          <ActivityList
            style={{
              flex: 1,
              // marginTop: 20,
            }}
            data={this.state.data}
            circleSize={35}
            circleColor='rgb(45,156,219)'
            lineColor='rgb(45,156,219)'
            descriptionStyle={{color: 'gray'}}
            options={{
              style: {paddingTop: 0},
              refreshControl: (
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.onRefresh}
                />
              ),
              renderFooter: this.renderFooter,
              onEndReached: this.onEndReached,
            }}
            innerCircle={'icon'}
            lineWidth={3}
            onEventPress={
              (rowdata) => navigation.dispatch(
                {type: NAV_ACTIVITIES_DETAIL_SCREEN}, rowdata)
            }
          />
        </CardSection>
      </BackgroundImageContainer>
    );
  }
}

ActivitiesScreen.navigationOptions = ({navigation}) => ({
  headerStyle: styles.headerCustomStyle,
  //headerLeft: <DefaultBackButton navigation={navigation}
  //                             buttonType={BUTTON_CUSTOM_HEADER_BACK}/>,
  headerRight: <BarButton navigation={navigation}/>,
  drawerIcon: ({tintColor}) => (
    <Icon name='format-list-bulleted'
          size={cssV('DRAWER_ICON_SIZE')}
          style={{color: tintColor}}
    />
  ),
});

const mapStateToProps = (state) => {
  const {user} = state.auth;
  // console.log('state', user);
  return {user};
};

export default connect(mapStateToProps, null)(ActivitiesScreen);