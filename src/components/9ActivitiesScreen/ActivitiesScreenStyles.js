/**
 * Created by rajanmaharjan on 7/11/17.
 */

import cssV from '../../styles/variables/cssV';

const ActivitiesScreenStyles = () => {
  return {
    activitiesScreenContainer: {
      flexGrow: 1,
      // backgroundColor: 'rgba(40, 40, 40, 0.1)',
    },
    activitiesScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    activitiesScreenImgLogo: {
      width: 200,
      height: 200,
    },
    activitiesScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 100,
    },
    activitiesScreenProfileImageBorder: {
      alignItems: 'center',
      justifyContent: 'center',
      width: 90,
      height: 90,
      backgroundColor: 'transparent',
      borderRadius: 100,
      borderColor: 'yellow',
      borderWidth: 3,
      opacity: 1,
    },
    activitiesScreenProfileImageComponent: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 45,
    },
    activitiesScreenProfileImageContainer: {
      // borderWidth: 1,
      alignItems: 'center',
      justifyContent: 'center',
      width: 74,
      height: 74,
      backgroundColor: 'transparent',
      borderRadius: 36,
      borderColor: 'transparent',
      shadowOpacity: 0,
    },
    activitiesScreenProfileImageStyle: {
      alignItems: 'center',
      justifyContent: 'center',
      width: 74,
      height: 74,
      borderRadius: 36,
      resizeMode: 'cover',
    },
    activitiesScreenIconImageStyle: {
      borderWidth: 1,
      alignItems: 'center',
      justifyContent: 'center',
      width: 40,
      height: 40,
      backgroundColor: 'transparent',
      borderRadius: 20,
      borderColor: 'transparent',
      resizeMode: 'cover',
      shadowOpacity: 0,
    },
    activitiesHeaderTextStyle: {
      fontSize: 18,
      color: '#f2b500',
      fontWeight: '400',
      paddingTop: 7,
      paddingBottom: 30,
    },
    activitiesHeaderCardStyle: {
      backgroundColor: '#0080ac',
      height: 35,
      justifyContent: 'space-around',
    },
  };
};

export default ActivitiesScreenStyles ;