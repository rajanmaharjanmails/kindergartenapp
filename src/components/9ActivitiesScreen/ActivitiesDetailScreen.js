/**
 * Created by rajanmaharjan on 9/9/17.
 */

import React, {
  Component,
} from 'react';

import {
  StyleSheet,
  Platform,
  View,
  Text,
  Image,
  ScrollView,
} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MyText from 'react-native-letter-spacing';
import Grid from 'react-native-grid-component';

import {
  MyStatusBar,
  HeaderComponent,
  ProfileImageComponent,
  BackgroundImageContainer,
  BarButton,
  DefaultBackButton,
} from '../1common';

import {
  NAV_SINGIN_SCREEN,
  NAV_CREATE_ACC_SCREEN,
} from '../../actions/types';

import {
  BUTTON_CUSTOM_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class ActivitiesDetailScreen extends Component {

  constructor () {
    super();

    this.state = {
      photos: [
        {
          id: 1,
          src: require('../../assets/icons/archery.png'),
        },
        {
          id: 1,
          src: require('../../assets/icons/archery.png'),
        },
        {
          id: 1,
          src: require('../../assets/icons/archery.png'),
        },
        {
          id: 1,
          src: require('../../assets/icons/archery.png'),
        },
        {
          id: 1,
          src: require('../../assets/icons/archery.png'),
        },
        {
          id: 1,
          src: require('../../assets/icons/archery.png'),
        },
      ],
    };
  }

  componentDidMount () {
    SplashScreen.hide();
    // console.warn(JSON.stringify(this.props));
  }

  _renderHeader () {

    const {
      activitiesDetailHeaderStyle,
      activitiesDetailScreenHeaderContainer,
      activitiesDetailProfileImageComponent,
      activitiesDetailScreenProfileImageStyle,
      activitiesDetailHeaderTextStyle,
      activitiesDetailHeaderTimeContainerStyle,
      activitiesDetailHeaderTimeIconStyle,
      activitiesDetailHeaderTextTimeStyle,
    } = styles;

    return (
      <HeaderComponent style={activitiesDetailHeaderStyle}>
        <View style={activitiesDetailScreenHeaderContainer}>
          <ProfileImageComponent
            style={activitiesDetailProfileImageComponent }>
            <Image
              source={require('../../assets/icons/lunch.png')}
              style={activitiesDetailScreenProfileImageStyle}
            />
            <Text
              style={activitiesDetailHeaderTextStyle}>
              Snacks
            </Text>
            <View
              style={activitiesDetailHeaderTimeContainerStyle}>
              <Icon name='clock'
                    size={15}
                    style={activitiesDetailHeaderTimeIconStyle}
              />
              <MyText
                letterSpacing={1}
                wordSpacing={5}
                style={activitiesDetailHeaderTextTimeStyle}>
                10:50 AM
              </MyText>
            </View>
          </ProfileImageComponent>
        </View>
      </HeaderComponent>
    );
  };

  _renderDescription () {

    const {
      activitiesDetailDescriptionContainer,
      activitiesDetailDescriptionStyle,
    } = styles;

    return (
      <MyText
        letterSpacing={1}
        wordSpacing={3}
        textAlign='justify'
        containerStyle={activitiesDetailDescriptionContainer}
        style={activitiesDetailDescriptionStyle}>
        Lorem ipsum dolor sit amet, et usu congue vocibus, ei, intelle gam
        uaerendum an nam. Vocibus apeiriannovis, eos cucongu emoo scaevola
        accusamus. Et seaplacerat persecutiioinn
      </MyText>
    );
  };

  _renderGallery (item, i) {
    return (
      <View style={{
        flex: 1,
        borderWidth: 1,
        borderColor: cssV('LIGHT_GRAY'),
        margin: 2,
        borderRadius: 10,
      }} key={i}>
        <Image
          resizeMode={Image.resizeMode.cover}
          style={{
            flex: 1,
            height: 100,
            width: 100,
          }}
          source={item.src}/>
      </View>
    );
  }

  render () {
    const {
      navigation,
    } = this.props;

    const {
      activitiesDetailScreenContainer,
      activitiesDetailScreenViewContainer,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={'rgba(1, 79, 112, 0.8)'}
                     barStyle="light-content"/>
        <KeyboardAwareScrollView
          contentContainerStyle={activitiesDetailScreenContainer}>
          {this._renderHeader()}
          <View style={activitiesDetailScreenViewContainer}>
            {this._renderDescription()}
            <View style={{marginTop: 25}}>
              <Text style={{
                color: cssV('LIGHT_TEAL'),
                fontSize: 16,
              }}>
                Gallery
              </Text>
              <View style={{marginTop: 10}}>
                <Grid
                  style={{flex: 1}}
                  renderItem={this._renderGallery.bind(this)}
                  data={this.state.photos}
                  itemsPerRow={3}
                />
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

ActivitiesDetailScreen.navigationOptions = ({navigation}) => ({
  headerStyle: styles.headerCustomStyle,
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_CUSTOM_HEADER_BACK}/>,
  headerRight: <BarButton navigation={navigation}/>,
  drawerIcon: ({tintColor}) => (
    <Icon name='format-list-bulleted'
          size={cssV('DRAWER_ICON_SIZE')}
          style={{color: tintColor}}
    />
  ),
});

export default ActivitiesDetailScreen;
