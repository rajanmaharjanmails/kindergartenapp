/**
 * Created by rajanmaharjan on 9/10/17.
 */
import cssV from '../../styles/variables/cssV';

const ActivitiesDetailScreenStyles = () => {
  return {
    activitiesDetailScreenContainer: {
      flexGrow: 1,
      // marginBottom: 30,
    },
    activitiesDetailScreenViewContainer: {
      padding: 10,
      marginBottom: 40,
    },
    activitiesDetailScreenHeaderContainer: {
      flex: 1,
      backgroundColor: cssV('DARK_TEAL'),
    },
    activitiesDetailHeaderStyle: {
      height: 180,
    },
    activitiesDetailDescriptionContainer: {
      alignItems: 'center',
    },
    activitiesDetailDescriptionStyle: {
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      color: cssV('EX_DARK_GRAY'),
      fontWeight: '400',
      alignContent: 'center',
      textAlign: 'center',
      fontSize: 20,
      alignSelf: 'stretch',
    },
    activitiesDetailHeaderContainer: {
      marginBottom: 20,
      marginTop: 100,
    },
    activitiesDetailProfileImageComponent: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 45,
    },
    activitiesDetailScreenProfileImageBorder: {
      alignItems: 'center',
      justifyContent: 'center',
      width: 100,
      height: 100,
      backgroundColor: 'transparent',
      borderRadius: 100,
      borderColor: 'yellow',
      borderWidth: 3,
      opacity: 1,
    },
    activitiesDetailScreenProfileImageStyle: {
      borderWidth: 1,
      alignItems: 'center',
      justifyContent: 'center',
      width: 74,
      height: 74,
      backgroundColor: 'transparent',
      borderRadius: 36,
      borderColor: 'transparent',
      resizeMode: 'cover',
    },
    activitiesDetailHeaderTextStyle: {
      fontSize: 18,
      color: cssV('LIGHT_WHITE'),
      fontWeight: '400',
      paddingTop: 5,
    },
    activitiesDetailHeaderTimeContainerStyle: {
      flexDirection: 'row',
      alignSelf: 'flex-start',
      paddingLeft: 10,
    },
    activitiesDetailHeaderTimeIconStyle: {
      paddingTop: 1,
      paddingRight: 5,
      color: cssV('LIGHT_WHITE'),
    },
    activitiesDetailHeaderTextTimeStyle: {
      fontSize: 15,
      fontFamily: cssV('CUSTOM_TEXT_FONT'),
      color: cssV('LIGHT_WHITE'),
      fontWeight: '400',
      paddingTop: 2,
    },
  };
};

export default ActivitiesDetailScreenStyles ;