/**
 * Created by rajanmaharjan on 8/31/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  StyleSheet,
  Platform,
  View,
  Text,
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Spinner from 'react-native-loading-spinner-overlay';

import {
  MyStatusBar,
  BackgroundImageContainer,
  BarButton,
  CommonInput,
  CustomButton,
  DefaultBackButton,
} from '../1common';

import {
  BUTTON_DEFAULT,
  BUTTON_CUSTOM_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import {
  studentCodeChanged,
  useStudentCode,
} from '../../actions';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class AddStdScreen extends Component {

  onStudentCodeChanged (text) {
    this.props.studentCodeChanged(text);
  }

  onUseButtonPress () {
    const {student_code} = this.props;
    this.props.useStudentCode({student_code});
  }

  render () {
    const {
      student_code, student_code_use, loading, error,
    } = this.props;

    const {
      addStdScreenContainer,
      addStdScreenViewContainer,
      addStdScreenCustomBtn,
      addStdScreenInputStyle,
      addStdScreenTextStyle,
      addStdScreenTextInput,
      addStdScreenErrorText,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={cssV('STATUS_BAR_COLOR')}
                     barStyle="light-content"/>
        <Spinner
          visible={loading && student_code_use && !error}
          animation='fade'
          overlayColor="rgba(1, 0, 10, 0.9)"
          textContent={'Getting student info...'}
          textStyle={{
            color: '#FFF',
            fontWeight: '300',
          }}/>
        <KeyboardAwareScrollView contentContainerStyle={addStdScreenContainer}>
          <View style={addStdScreenViewContainer}>
            <Text style={addStdScreenTextStyle}>
              Enter Student Code
            </Text>
            <CommonInput
              placeholder="Code"
              placeholderTextColor="#1689a8"
              onChangeText={this.onStudentCodeChanged.bind(this)}
              autoCapitalize="characters"
              value={student_code}
              style={addStdScreenInputStyle}
              textInputStyle={addStdScreenTextInput}
            />
            <Text style={addStdScreenErrorText}>
              {error}
            </Text>
            <CustomButton
              onPress={this.onUseButtonPress.bind(this)}
              buttonType={BUTTON_DEFAULT}
              style={addStdScreenCustomBtn}
            >
              Use
            </CustomButton>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

AddStdScreen.navigationOptions = ({navigation}) => ({
  title: 'Add Student',
  headerTitleStyle: {
    color: cssV('PRIMARY_WHITE'),
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
  },
  headerStyle: {
    backgroundColor: cssV('DARK_TEAL'),
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_CUSTOM_HEADER_BACK}/>,
  headerRight: <BarButton navigation={navigation}/>,
  drawerLabel: 'Accounts',
  drawerIcon: ({tintColor}) => (
    <Icon name='account-switch'
          size={cssV('DRAWER_ICON_SIZE')}
          style={{color: tintColor}}
    />
  ),
});

const mapStateToProps = ({addStudent}) => {
  const {student_code, student_code_use, error, loading} = addStudent;
  return {student_code, student_code_use, error, loading};
};

export default connect(mapStateToProps, {
  studentCodeChanged, useStudentCode,
})(AddStdScreen);