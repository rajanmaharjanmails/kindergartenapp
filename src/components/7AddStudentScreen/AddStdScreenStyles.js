/**
 * Created by rajanmaharjan on 8/31/17.
 */

import cssV from '../../styles/variables/cssV';

const AddStdScreenStyles = () => {
  return {
    addStdScreenContainer: {
      flexGrow: 1,
    },
    addStdScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 100,
      marginTop: 100,
    },
    addStdScreenCustomBtn: {
      marginTop: 40,
      backgroundColor: cssV('LIGHT_TEAL'),
      borderColor: cssV('LIGHT_TEAL'),
    },
    addStdScreenTextStyle: {
      color: cssV('LIGHT_TEAL'),
      fontSize: 20,
      marginBottom: 20,
    },
    addStdScreenInputStyle: {
      borderColor: cssV('LIGHT_TEAL'),
      marginRight: 60,
      marginLeft: 60,
    },
    addStdScreenTextInput: {
      color: cssV('LIGHT_TEAL'),
    },
    addStdScreenErrorText: {
      marginTop: 20,
      fontSize: 20,
      alignSelf: 'center',
      color: 'red',
    },
  };
};

export default AddStdScreenStyles;