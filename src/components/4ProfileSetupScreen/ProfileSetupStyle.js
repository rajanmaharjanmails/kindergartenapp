/**
 * Created by rajanmaharjan on 8/24/17.
 */

import cssV from '../../styles/variables/cssV';

const ProfileSetupScreenStyles = () => {
  return {
    profileSetupScreenContainer: {
      flexGrow: 1,
    },
    profileSetupScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 40,
      marginBottom:40
    },
    profileSetupScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 40,
    },
    profileSetupTextStyle: {
      fontSize: 20,
      marginTop:40,
      marginBottom: 20,
    },
    profileSetupScreenErrorText: {
      marginTop: 20,
      fontSize: 20,
      alignSelf: 'center',
      color: 'red',
    },
  };
};

export default ProfileSetupScreenStyles ;