/**
 * Created by rajanmaharjan on 8/24/17.
 */

import React, { Component }from 'react';
import { connect } from 'react-redux';

import {
  StyleSheet, View, Text,
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';

import {
  MyStatusBar,
  BackgroundImageContainer,
  CustomButton,
  CommonInput,
  DefaultBackButton,
} from '../1common';

import {
  NAV_ACTIVITIES_SCREEN,
} from '../../actions/types';

import {
  BUTTON_DEFAULT,
  BUTTON_TRANSPARENT,
  BUTTON_DEFAULT_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import {
  profileNameChanged,
  profileNumChanged,
  profileAddressChanged,
  updateUserProfile,
} from '../../actions';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class ProfileSetupScreen extends Component {

  onNameChanged (text) {
    this.props.profileNameChanged(text);
  }

  onPhoneChange (text) {
    this.props.profileNumChanged(text);
  }

  onAddressChange (text) {
    this.props.profileAddressChanged(text);
  }

  onSaveButtonPress () {
    const {user_name, user_phone, user_address} = this.props;
    this.props.updateUserProfile({user_name, user_phone, user_address});
  }

  onSkipButtonPress () {
    const {navigation} = this.props;
    navigation.dispatch({type: NAV_ACTIVITIES_SCREEN});
  }

  render () {
    const {user_name, user_phone, user_address, error, loading} = this.props;

    const {
      profileSetupScreenContainer,
      profileSetupScreenViewContainer,
      profileSetupScreenCustomBtn,
      profileSetupTextStyle,
      profileSetupScreenErrorText,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={1}
      >
        <MyStatusBar backgroundColor={'rgba(0, 0, 0, 0.2)'}
                     barStyle="light-content"/>
        <Spinner
          visible={loading && !error}
          animation='fade'
          overlayColor="rgba(1, 0, 10, 0.9)"
          textContent={'Updating profile info...'}
          textStyle={{
            color: '#FFF',
            fontWeight: '300',
          }}/>
        <KeyboardAwareScrollView
          contentContainerStyle={profileSetupScreenContainer}>
          <View style={profileSetupScreenViewContainer}>
            <CommonInput
              placeholder="Full Name"
              iconName="clipboard-account"
              autoCapitalize="words"
              onChangeText={this.onNameChanged.bind(this)}
              value={user_name}
              leftIcon={true}
            />
            <CommonInput
              placeholder="Phone Number"
              iconName="phone"
              keyboardType="numeric"
              onChangeText={this.onPhoneChange.bind(this)}
              value={user_phone}
              leftIcon={true}
            />
            <CommonInput
              placeholder="Address"
              iconName="home-map-marker"
              autoCapitalize="words"
              onChangeText={this.onAddressChange.bind(this)}
              value={user_address}
              leftIcon={true}
            />
            <Text style={profileSetupScreenErrorText}>
              {error}
            </Text>
            <CustomButton
              onPress={this.onSaveButtonPress.bind(this)}
              buttonType={BUTTON_DEFAULT}
              style={profileSetupScreenCustomBtn}
            >
              Save
            </CustomButton>
            <CustomButton
              onPress={this.onSkipButtonPress.bind(this)}
              buttonType={BUTTON_TRANSPARENT}
            >
              Skip
            </CustomButton>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

ProfileSetupScreen.navigationOptions = ({navigation}) => ({
  headerStyle: styles.headerStyle,
  title: 'Personal Information',
  headerTintColor: '#fefefe',
  headerTitleStyle: {
    color: '#fefefe',
    fontWeight: '300',
    alignSelf: 'center',
    // fontFamily: 'KidsBook',
  },
  // headerLeft: <DefaultBackButton navigation={navigation}
  //                                buttonType={BUTTON_DEFAULT_HEADER_BACK}/>,
});

const mapStateToProps = ({profileUpdate}) => {
  const {user_name, user_phone, user_address, error, loading} = profileUpdate;
  return {user_name, user_phone, user_address, error, loading};
};

export default connect(mapStateToProps, {
  profileNameChanged,
  profileNumChanged,
  profileAddressChanged,
  updateUserProfile,
})(ProfileSetupScreen);