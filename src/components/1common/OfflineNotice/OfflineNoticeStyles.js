/**
 * Created by rajanmaharjan on 7/30/17.
 */

import cssV from '../../../styles/variables/cssV';
import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

const OfflineNoticeStyles = () => {
    return {
        offlineContainer: {
            backgroundColor: cssV('RED'),
            height: 30,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            width,
            position: 'absolute',
            bottom: 0
        },
        offlineText: {
            color: cssV('PRIMARY_WHITE')
        }
    };
};

export default OfflineNoticeStyles;