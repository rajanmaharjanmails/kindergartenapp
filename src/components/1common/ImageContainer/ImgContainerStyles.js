/**
 * Created by rajanmaharjan on 7/30/17.
 */

const CommonImageContainerStyles = () => {
  return {
    bgImageMainContainer: {
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0)',
      shadowOpacity: 0,
    },
    bgImageChildContainer: {
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0)',
      shadowOpacity: 0,
    },
    bgImageContainer: {
      flex: 1,
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    bgImageContainerStyle: {
      flex: 1,
      width: undefined,
      height: undefined,
      backgroundColor: 'rgba(0,0,0,0)',
      shadowOpacity: 0,
    },
    bgImageStyle: {
      resizeMode: 'cover',
    },
    BgImageText: {
      textAlign: 'center',
      color: 'white',
      backgroundColor: 'rgba(0,0,0,0)',
      shadowOpacity: 0,
      fontSize: 32,
    },
  };
};

export default CommonImageContainerStyles;