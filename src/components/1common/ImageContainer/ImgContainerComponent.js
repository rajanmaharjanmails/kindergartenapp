/**
 * Created by rajanmaharjan on 7/30/17.
 */

import React from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
} from 'react-native';

import PropTypes from 'prop-types';

import CommonImageContainerStyles from './ImgContainerStyles';
const styles = StyleSheet.create(CommonImageContainerStyles());

const BackgroundImageContainer = (props) => {

  const {bgImageMainContainer, bgImageContainer, bgImageContainerStyle, bgImageStyle} = styles;
  const {children, imageNumber, mainContainerStyle, imageContainer, imageStyle} = props;

  let image = require('../../../assets/images/bgStdTeacher.png');
  switch (imageNumber) {
    case 1:
      image = require('../../../assets/images/bgStdTeacher.png');
      break;
    case 2:
      image = require('../../../assets/images/bgGrass.png');
      break;
    case 3:
      image = require('../../../assets/images/bgTeacherStdHeader.png');
      break;
    case 4:
      image = require('../../../assets/images/bgAbcHeader.png');
      break;
    default:
      image = require('../../../assets/images/bgStdTeacher.png');
      break;
  }

  return (
    <View
      style={[bgImageMainContainer, mainContainerStyle]}
    >
      <View
        style={[bgImageContainer, imageContainer]}
      >
        <ImageBackground
          style={[bgImageContainerStyle]}
          imageStyle={[bgImageStyle, imageStyle]}
          source={image}
        />
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}
      >
        {children}
      </View>
    </View>
  );
};

export { BackgroundImageContainer };

BackgroundImageContainer.propTypes = {
  children: PropTypes.any,
};
