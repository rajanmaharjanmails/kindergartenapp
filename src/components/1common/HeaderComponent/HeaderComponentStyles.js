/**
 * Created by rajanmaharjan on 7/30/17.
 */

import cssV from '../../../styles/variables/cssV';

const containerHeight = 160;
const containerBg = 'transparent';

const CommonHeaderStyles = () => {
  return {
    headerContainer: {
      flexDirection: 'row',
      backgroundColor: containerBg,
      height: containerHeight,
      elevation: 2,
      position: 'relative',
    }
  };
};

export default CommonHeaderStyles ;