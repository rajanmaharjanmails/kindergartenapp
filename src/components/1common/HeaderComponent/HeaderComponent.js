/**
 * Created by rajanmaharjan on 7/29/17.
 */
import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import PropTypes from 'prop-types';

import CommonHeaderStyles from './HeaderComponentStyles';

const styles = StyleSheet.create(CommonHeaderStyles());

const HeaderComponent = (props) => {
  const {children, style} = props;

  return (
    <View style={[styles.headerContainer, style]}>
      {children}
    </View>
  );
};

export { HeaderComponent };

HeaderComponent.propTypes = {
  children: PropTypes.any,
};
