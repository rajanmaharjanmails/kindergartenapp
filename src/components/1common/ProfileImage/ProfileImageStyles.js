/**
 * Created by rajanmaharjan on 7/30/17.
 */

import cssV from '../../../styles/variables/cssV';

const CommonProfileImageStyles = () => {
  return {
    profileImageContainer: {
      alignItems: 'center',
    }
  };
};

export default CommonProfileImageStyles ;