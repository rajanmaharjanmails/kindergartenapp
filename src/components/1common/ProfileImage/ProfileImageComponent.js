/**
 * Created by rajanmaharjan on 7/29/17.
 */

import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import PropTypes from 'prop-types';

import CommonProfileImageStyles from './ProfileImageStyles';

const styles = StyleSheet.create(CommonProfileImageStyles());

const ProfileImageComponent = (props) => {

  const {children, style} = props;

  return (
    <View
      style={[styles.profileImageContainer, style]}
    >
      {children}
    </View>
  );
};

export {ProfileImageComponent};

ProfileImageComponent.propTypes = {
  children: PropTypes.any,
};
