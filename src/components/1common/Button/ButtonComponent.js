/**
 * Created by rajanmaharjan on 7/11/17.
 */

import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native';

import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
  BUTTON_DEFAULT,
  BUTTON_TRANSPARENT,
  BUTTON_NEXT,
} from './ButtonTypes';

import CommonButtonStyles from './ButtonStyles';
const styles = StyleSheet.create(CommonButtonStyles());

const CustomButton = (props) => {
  const {commonButtonDefault, commonButtonTransparent, commonButtonContainerStyle, commonButtonText, commonButtonIconStyle} = styles;
  const {onPress, children, style, buttonType} = props;

  switch (buttonType) {
    case BUTTON_DEFAULT:
      return (
        <TouchableOpacity
          onPress={onPress}
          style={[commonButtonDefault, style]}
        >
          <Text style={commonButtonText}>{children}</Text>
        </TouchableOpacity>
      );
      break;
    case BUTTON_TRANSPARENT:
      return (
        <TouchableOpacity
          onPress={onPress}
          style={[commonButtonTransparent, style]}
        >
          <Text style={commonButtonText}>{children}</Text>
        </TouchableOpacity>
      );
      break;
    case BUTTON_NEXT:
      return (
        <TouchableOpacity
          onPress={onPress}
          style={[commonButtonDefault, style]}
        >
          <View style={commonButtonContainerStyle}>
            <Text style={commonButtonText}
            >{children}</Text>
            <Icon
              name='chevron-right'
              size={24}
              color="white"
              style={commonButtonIconStyle}
            />
          </View>
        </TouchableOpacity>
      );
      break;
    default:
      return (
        <TouchableOpacity
          onPress={onPress}
          style={[commonButtonDefault, style]}
        >
          <Text style={commonButtonText}>{children}</Text>
        </TouchableOpacity>
      );
      break;
  }
};

export { CustomButton } ;

CustomButton.propTypes = {
  children: PropTypes.any,
  onPress: PropTypes.func,
};

