/**
 * Created by rajanmaharjan on 8/19/17.
 */

export const BUTTON_DEFAULT = 'button_default';
export const BUTTON_TRANSPARENT = 'button_transparent';
export const BUTTON_NEXT = 'button_next';
export const BUTTON_DEFAULT_HEADER_BACK = 'button__header_default_back';
export const BUTTON_CUSTOM_HEADER_BACK = 'button__header_custom_back';

