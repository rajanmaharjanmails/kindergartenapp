/**
 * Created by rajanmaharjan on 7/29/17.
 */
import React from 'react';
import {
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';

import CommonButtonStyles from './ButtonStyles';
const styles = StyleSheet.create(CommonButtonStyles());

const BarButton = ({navigation}) => (
    <TouchableOpacity
      onPress={() => navigation.navigate('DrawerOpen')}
      style={styles.headerButtonRight}
    >
      <Image
        source={require('../../../assets/icons/barButton.png')}
        style={{flex: 1}}
        resizeMode="contain"
      />
    </TouchableOpacity>
  )
;

export { BarButton };

BarButton.propTypes = {
  navigation: PropTypes.object.isRequired,
};

