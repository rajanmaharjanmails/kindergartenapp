/**
 * Created by rajanmaharjan on 7/11/17.
 */

import cssV from '../../../styles/variables/cssV';
import {
  Platform,
} from 'react-native';

const CommonButtonStyles = () => {
  return {
    commonButtonDefault: {
      alignSelf: 'stretch',
      backgroundColor: cssV('PRIMARY_BLUE'),
      borderColor: cssV('PRIMARY_BLUE'),
      borderWidth: 1,
      borderRadius: 5,
      marginLeft: 35,
      marginRight: 35,
    },
    commonButtonTransparent: {
      alignSelf: 'stretch',
      backgroundColor: 'transparent',
      shadowOpacity: 0,
      borderRadius: 5,
      borderWidth: 1,
      borderColor: cssV('PRIMARY_WHITE'),
      marginLeft: 35,
      marginRight: 35,
    },
    commonButtonContainerStyle: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'stretch',
      alignSelf: 'stretch',
    },
    commonButtonText: {
      alignSelf: 'center',
      color: cssV('PRIMARY_WHITE'),
      fontSize: 16,
      fontWeight: '300',
      paddingTop: 10,
      paddingBottom: 10,
      fontFamily: 'Sniglet-Regular' //'PapaBear'
    },
    commonButtonIconStyle: {
      height: 25,
      width: 25,
      marginTop: Platform.OS === 'ios' ? 8 : 6.6,
    },
    headerDefaultBackButton: {
      marginLeft: 5,
      alignItems: 'center',
      justifyContent: 'center',
    },
    headerCustomBackButton: {
      marginTop: Platform.OS === 'ios' ? 10 : 35,
      marginLeft: 15,
      alignItems: 'center',
      justifyContent: 'center',
      width: 38,
      height: 38,
      backgroundColor: 'transparent',
      shadowOpacity: 0.2,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      marginBottom: 7,
    },
    headerButtonRight: {
      marginTop: Platform.OS === 'ios' ? 10 : 35,
      marginRight: 15,
      alignItems: 'center',
      justifyContent: 'center',
      width: 38,
      height: 38,
      backgroundColor: 'transparent',
      shadowOpacity: 0.2,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      marginBottom: 7,
    },
  };
};

export default CommonButtonStyles;