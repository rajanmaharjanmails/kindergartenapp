/**
 * Created by rajanmaharjan on 8/23/17.
 */

import React from 'react';
import { Image, Text } from 'react-native';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  NAV_BACK,
}from '../../../actions/types';

import {
  BUTTON_DEFAULT_HEADER_BACK,
  BUTTON_CUSTOM_HEADER_BACK,
} from './ButtonTypes';

import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import CommonButtonStyles from './ButtonStyles';
const styles = StyleSheet.create(CommonButtonStyles());

const DefaultBackButton = (props) => {
  const {headerDefaultBackButton, headerCustomBackButton} = styles;
  const {style, buttonType, navigation} = props;

  switch (buttonType) {
    case BUTTON_DEFAULT_HEADER_BACK:
      return (
        <TouchableOpacity
          onPress={() => navigation.dispatch({type: NAV_BACK})}
          style={[headerDefaultBackButton, style]}
        >
          <Icon name="chevron-left" size={35} color="white"/>
        </TouchableOpacity>
      );
      break;
    case BUTTON_CUSTOM_HEADER_BACK:
      return (
        <TouchableOpacity
          onPress={() => navigation.dispatch({type: NAV_BACK})}
          style={headerCustomBackButton}
        >
          <Image
            source={require('../../../assets/icons/customBackButton.png')}
            style={{flex: 1}}
            resizeMode="contain"
          />
        </TouchableOpacity>
      );
      break;
    default:
      return (
        <TouchableOpacity
          onPress={() => navigation.dispatch({type: NAV_BACK})}
          style={[headerDefaultBackButton, style]}
        >
          <Icon name="chevron-left" size={35} color="white"/>
        </TouchableOpacity>
      );
      break;
  }
};

export { DefaultBackButton };

DefaultBackButton.propTypes = {
  navigation: PropTypes.object.isRequired,
};

