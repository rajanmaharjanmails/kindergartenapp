/**
 * Created by rajanmaharjan on 7/11/17.
 */

import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';

import PropTypes from 'prop-types';

import CommonCardStyles from './CardComponentStyles';

const styles = StyleSheet.create(CommonCardStyles());

const Card = (props) => {
  return (
    <View style={[styles.cardContainerStyle, props.style]}>
      {props.children}
    </View>
  );
};

export {Card};

Card.propTypes = {
  children: PropTypes.any,
};
