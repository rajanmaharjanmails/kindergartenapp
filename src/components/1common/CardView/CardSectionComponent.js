/**
 * Created by rajanmaharjan on 7/11/17.
 */

import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import PropTypes from 'prop-types';
import CommonCardStyles from './CardComponentStyles';
const styles = StyleSheet.create(CommonCardStyles());

const CardSection = (props) => {
  return (
    <View style={[styles.cardSectionContainer, props.style]}>
      {props.children}
    </View>
  );
};

export { CardSection };

CardSection.propTypes = {
  children: PropTypes.node,
};
