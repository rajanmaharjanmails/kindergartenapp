/**
 * Created by rajanmaharjan on 7/30/17.
 */

import cssV from '../../../styles/variables/cssV';

const CommonCardStyles = () => {
  return {
    cardContainerStyle: {
      // borderWidth: 1,
      borderRadius: 2,
      borderColor: cssV('LIGHT_WHITE'),
      borderBottomWidth: 0,
      shadowColor: cssV('LIGHT_BLACK'),
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.1,
      elevation: 1,
      marginLeft: 10,
      marginRight: 10,
      marginTop: 12,
    },
    cardSectionContainer: {
      // borderBottomWidth: 1,
      // padding: 15,
      backgroundColor: 'transparent',
      justifyContent: 'flex-start',
      flexDirection: 'row',
      borderColor: cssV('LIGHT_WHITE'),
      position: 'relative',
    },
  };
};

export default CommonCardStyles ;