/**
 * Created by rajanmaharjan on 7/11/17.
 */

import cssV from '../../../styles/variables/cssV';

const CommonInputStyles = () => {
  return {
    commonInputContainerStyle: {
      flexDirection: 'row',
      height: 48,
      borderColor: cssV('LIGHT_WHITE'),
      borderBottomWidth: 0.5,
      marginLeft: 35,
      marginRight: 35,
      marginTop: 15,
    },
    commonInputStyle: {
      flex: 1,
      color: cssV('WHITE'),
      textAlign: 'center',
      fontSize: 26,
      paddingRight: 25,
      fontFamily: cssV('INPUT_TEXT_FONT'),
    }
  };
};

export default CommonInputStyles;