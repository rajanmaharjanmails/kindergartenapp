/**
 * Created by rajanmaharjan on 8/22/17.
 */

import  React from 'react';
import {
  TextInput,
  StyleSheet,
  View,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import CommonInputStyles  from './InputStyles';

const styles = StyleSheet.create(CommonInputStyles());

const CommonInput = (props) => {

  const {
    value, onChangeText,
    placeholder, placeholderTextColor,
    autoCorrect, autoCapitalize,
    secureTextEntry, keyboardType,
    iconName, style, textInputStyle,
    iconColor, leftIcon, rightIcon,
    iconStyle, editable, maxLength,
  } = props;

  const {commonInputContainerStyle, commonInputStyle} = styles;

  const renderIconLeft = () => (
    <Icon name={iconName}
          size={20}
          color={iconColor ? iconColor : 'white'}
          style={{paddingTop: 5}}/>
  );

  const renderIconRight = () => (
    <Icon name={iconName}
          size={20}
          color={iconColor ? iconColor : 'white'}
          style={[{paddingTop: 5}, iconStyle]}/>
  );

  return (
    <View style={[commonInputContainerStyle, style]}>
      {leftIcon ? renderIconLeft() : null}
      <TextInput
        {...this.props}
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        autoCorrect={autoCorrect}
        style={[commonInputStyle, textInputStyle]}
        value={value}
        onChangeText={onChangeText}
        autoCapitalize={autoCapitalize}
        placeholderTextColor={placeholderTextColor
          ? placeholderTextColor
          : '#999'}
        keyboardType={keyboardType}
        editable={editable}
        maxLength={maxLength}
      />
      {rightIcon ? renderIconRight() : null }
    </View>
  );
};

export { CommonInput };
