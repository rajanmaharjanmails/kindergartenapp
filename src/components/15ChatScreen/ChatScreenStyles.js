/**
 * Created by rajanmaharjan on 9/10/17.
 */
import cssV from '../../styles/variables/cssV';

const ChatScreenStyles = () => {
  return {
    chatScreenContainer: {
      flexGrow: 1,
    },
    chatScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    chatScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 100,
    },
  };
};

export default ChatScreenStyles;