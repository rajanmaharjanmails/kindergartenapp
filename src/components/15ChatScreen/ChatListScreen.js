/**
 * Created by rajanmaharjan on 9/2/17.
 */
import React, {
  Component,
} from 'react';

import {
  StyleSheet,
  Platform,
  View,
  Text,
} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
  MyStatusBar,
  BackgroundImageContainer,
  CustomButton,
  BarButton,
  DefaultBackButton,
} from '../1common';

import {
  NAV_CHAT_SCREEN
} from '../../actions/types';

import {
  BUTTON_DEFAULT,
  BUTTON_CUSTOM_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import cssV from '../../styles/variables/cssV';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class ChatListScreen extends Component {

  componentDidMount () {
    SplashScreen.hide();
  }

  render () {
    const {
      navigation,
    } = this.props;

    const {
      chatListScreenContainer,
      chatListScreenViewContainer,
      chatListScreenCustomBtn
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={2}
      >
        <MyStatusBar backgroundColor={'rgba(1, 79, 112, 0.8)'}
                     barStyle="light-content"/>
        <KeyboardAwareScrollView
          contentContainerStyle={chatListScreenContainer}>
          <View style={chatListScreenViewContainer}>
            <Text style={{color: '#026188'}}>
              CHAT LIST
            </Text>
            <CustomButton
              onPress={() => navigation.dispatch(
                {type: NAV_CHAT_SCREEN})}
              buttonType={BUTTON_DEFAULT}
              style={chatListScreenCustomBtn}
            >
              Chat Screen
            </CustomButton>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

ChatListScreen.navigationOptions = ({navigation}) => ({
  title: 'Chats',
  headerTitleStyle: {
    color: '#fff',
    fontWeight: '400',
    alignSelf: 'center',
    paddingTop: (Platform.OS === 'ios' ? 5 : 30),
  },
  headerStyle: {
    backgroundColor: '#026188',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 75,
  },
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_CUSTOM_HEADER_BACK}/>,
  headerRight: <BarButton navigation={navigation}/>,
  drawerIcon: ({tintColor}) => (
    <Icon name='wechat'
          size={cssV('DRAWER_ICON_SIZE')}
          style={{color: tintColor}}
    />
  ),
});

export default ChatListScreen;
