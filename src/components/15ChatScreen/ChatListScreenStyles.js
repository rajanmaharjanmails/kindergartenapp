/**
 * Created by rajanmaharjan on 9/2/17.
 */
import cssV from '../../styles/variables/cssV';

const ChatListScreenStyles = () => {
  return {
    chatListScreenContainer: {
      flexGrow: 1,
    },
    chatListScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    chatListScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 100,
      backgroundColor: cssV('DARK_TEAL'),
    },
  };
};

export default ChatListScreenStyles;