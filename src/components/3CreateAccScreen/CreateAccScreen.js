/**
 * Created by rajanmaharjan on 8/19/17.
 */

import React, { Component }from 'react';
import { connect } from 'react-redux';

import {
  StyleSheet, View, Text,
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';

import {
  MyStatusBar,
  BackgroundImageContainer,
  CustomButton,
  CommonInput,
  DefaultBackButton,
} from '../1common';

import {
  BUTTON_NEXT,
  BUTTON_DEFAULT_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import {
  regStudentCodeChanged,
  regEmailChanged,
  regPasswordChanged,
  regRePasswordChanged,
  createUserAcc,
} from '../../actions';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class CreateAccountScreen extends Component {

  onStudentCodeChanged (text) {
    this.props.regStudentCodeChanged(text);
  }

  onEmailChange (text) {
    this.props.regEmailChanged(text);
  }

  onPasswordChange (text) {
    this.props.regPasswordChanged(text);
  }

  onRePasswordChange (text) {
    this.props.regRePasswordChanged(text);
  }

  onButtonPress () {
    const {reg_code, reg_email, reg_password, reg_re_password} = this.props;

    this.props.createUserAcc(
      {reg_code, reg_email, reg_password, reg_re_password});
  }

  render () {
    const {
      reg_code, reg_email, reg_password, reg_re_password, reg_error, reg_loading,
    } = this.props;

    const {
      createAccScreenContainer,
      createAccScreenViewContainer,
      createAccScreenCustomBtn,
      createAccScreenErrorText,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={1}
      >
        <MyStatusBar backgroundColor={'rgba(0, 0, 0, 0.2)'}
                     barStyle="light-content"/>
        <Spinner
          visible={reg_loading && !reg_error}
          animation='fade'
          overlayColor="rgba(1, 0, 10, 0.9)"
          textContent={'Creating user...'}
          textStyle={{
            color: '#FFF',
            fontWeight: '300',
          }}/>
        <KeyboardAwareScrollView
          contentContainerStyle={createAccScreenContainer}>
          <View style={createAccScreenViewContainer}>
            <CommonInput
              placeholder="Student Code"
              iconName="account-card-details"
              onChangeText={this.onStudentCodeChanged.bind(this)}
              autoCapitalize="characters"
              value={reg_code}
              leftIcon={true}
            />
            <CommonInput
              placeholder="Email Address"
              keyboardType="email-address"
              onChangeText={this.onEmailChange.bind(this)}
              iconName="email-outline"
              autoCapitalize="none"
              value={reg_email}
              leftIcon={true}
            />
            <CommonInput
              placeholder="Password"
              secureTextEntry={true}
              iconName="lock-outline"
              onChangeText={this.onPasswordChange.bind(this)}
              value={reg_password}
              leftIcon={true}
            />
            <CommonInput
              placeholder="Confirm password"
              secureTextEntry={true}
              iconName="lock-outline"
              onChangeText={this.onRePasswordChange.bind(this)}
              value={reg_re_password}
              leftIcon={true}
            />
            <Text style={createAccScreenErrorText}>
              {reg_error}
            </Text>
            <CustomButton
              onPress={this.onButtonPress.bind(this)}
              buttonType={BUTTON_NEXT}
              style={createAccScreenCustomBtn}
            >
              Next
            </CustomButton>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

CreateAccountScreen.navigationOptions = ({navigation}) => ({
  headerStyle: styles.headerStyle,
  title: 'Create Account',
  headerTintColor: '#fefefe',
  headerTitleStyle: styles.headerTitleStyle,
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_DEFAULT_HEADER_BACK}/>,
});

const mapStateToProps = ({createUser}) => {
  const {
    reg_code, reg_email, reg_password, reg_re_password, reg_error, reg_loading,
  } = createUser;

  return {
    reg_code, reg_email, reg_password, reg_re_password, reg_error, reg_loading,
  };
};

export default connect(mapStateToProps, {
  regStudentCodeChanged,
  regEmailChanged,
  regPasswordChanged,
  regRePasswordChanged,
  createUserAcc,
})(CreateAccountScreen);