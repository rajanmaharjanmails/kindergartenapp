/**
 * Created by rajanmaharjan on 7/11/17.
 */
import {
  Dimensions,
} from 'react-native';

import cssV from '../../styles/variables/cssV';

const SCREEN_WIDTH = Dimensions.get('window').width;

const CreateAccScreenStyles = () => {
  return {
    createAccScreenContainer: {
      flexGrow: 1,
    },
    createAccScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 50,
    },
    createAccScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 40,
      paddingLeft: 30,
    },
    createAccScreenErrorText: {
      marginTop: 20,
      fontSize: 20,
      alignSelf: 'center',
      color: 'red',
    },
  };
};

export default CreateAccScreenStyles ;