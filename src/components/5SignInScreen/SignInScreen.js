/**
 * Created by rajanmaharjan on 8/19/17.
 */

import React, { Component }from 'react';
import { connect } from 'react-redux';

import {
  StyleSheet, Image, View, Text,
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';

import {
  MyStatusBar,
  BackgroundImageContainer,
  CustomButton,
  CommonInput,
  DefaultBackButton,
} from '../1common';

import {
  BUTTON_DEFAULT,
  BUTTON_DEFAULT_HEADER_BACK,
} from '../1common/Button/ButtonTypes';

import { emailChanged, passwordChanged, loginUser } from '../../actions';

import stylesImport from '../../styles/styles';

const styles = StyleSheet.create(stylesImport());

class SignInScreen extends Component {
  constructor () {
    super();
  }

  onEmailChange (text) {
    this.props.emailChanged(text);
  }

  onPasswordChange (text) {
    this.props.passwordChanged(text);
  }

  onButtonPress () {
    const {email, password} = this.props;
    this.props.loginUser({email, password});
  }

  render () {
    const {email, password, error, loading} = this.props;
    const {
      signInScreenContainer,
      signInScreenViewContainer,
      signInScreenImgContainer,
      signInScreenImgLogo,
      signInScreenCustomBtn,
      signInScreenErrorText,
    } = styles;

    return (
      <BackgroundImageContainer
        imageNumber={1}
      >
        <MyStatusBar backgroundColor={'rgba(0, 0, 0, 0.2)'}
                     barStyle="light-content"/>
        <Spinner
          visible={loading && !error}
          animation='fade'
          overlayColor="rgba(1, 0, 10, 0.9)"
          textContent={'Signing In...'}
          textStyle={{
            color: '#FFF',
            fontWeight: '300',
          }}/>
        <KeyboardAwareScrollView
          contentContainerStyle={signInScreenContainer}
        >
          <View style={signInScreenViewContainer}>
            <View style={signInScreenImgContainer}>
              <Image
                source={require(
                  '../../assets/logo/Kindergarten-logoname.png')}
                style={signInScreenImgLogo}
              />
            </View>
            <CommonInput
              placeholder="Email"
              keyboardType="email-address"
              iconName="account"
              onChangeText={this.onEmailChange.bind(this)}
              autoCapitalize="none"
              value={email}
              leftIcon={true}
            />
            <CommonInput
              placeholder="Password"
              secureTextEntry={true}
              iconName="lock-outline"
              onChangeText={this.onPasswordChange.bind(this)}
              leftIcon={true}
              value={password}
            />
            <Text style={signInScreenErrorText}>
              {error}
            </Text>
            <CustomButton
              onPress={this.onButtonPress.bind(this)}
              buttonType={BUTTON_DEFAULT}
              style={signInScreenCustomBtn}
            >
              Sign In
            </CustomButton>
          </View>
        </KeyboardAwareScrollView>
      </BackgroundImageContainer>
    );
  }
}

SignInScreen.navigationOptions = ({navigation}) => ({
  headerStyle: styles.headerStyle,
  title: 'Sing In',
  headerTintColor: '#fefefe',
  headerTitleStyle: styles.headerTitleStyle,
  headerLeft: <DefaultBackButton navigation={navigation}
                                 buttonType={BUTTON_DEFAULT_HEADER_BACK}/>,
});

const mapStateToProps = ({auth}) => {
  const {email, password, error, loading} = auth;
  return {email, password, error, loading};
};

export default connect(mapStateToProps, {
  emailChanged, passwordChanged, loginUser,
})(SignInScreen);