/**
 * Created by rajanmaharjan on 7/11/17.
 */

import cssV from '../../styles/variables/cssV';

const SignInScreenStyles = () => {
  return {
    signInScreenContainer: {
      flexGrow: 1,
    },
    signInScreenViewContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    signInScreenImgContainer: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    signInScreenImgLogo: {
      width: 200,
      height: 200,
    },
    signInScreenCustomBtn: {
      marginBottom: 20,
      marginTop: 30,
    },
    signInScreenErrorText: {
      marginTop: 20,
      fontSize: 20,
      alignSelf: 'center',
      color: 'red',
    },
  };
};

export default SignInScreenStyles ;