/**
 * Created by rajanmaharjan on 8/19/17.
 */

import { applyMiddleware, createStore, compose } from 'redux';
import { offline } from '@redux-offline/redux-offline';
import ReduxThunk from 'redux-thunk';
import AppReducer from '../reducers/';
import offlineConfig from '@redux-offline/redux-offline/lib/defaults';

// const config = {
//   ...offlineConfig,
//   persistOptions: {
//     // storage: AsyncStorage,
//     whitelist: ['studentList', 'notices', 'calendarActivities'],
//     debounce: 300,
//   }
// };
const composeEnhancers = window.REDUX_DEVTOOLS_EXTENSION_COMPOSE || compose;

const middlewares = [ReduxThunk];

if (process.env.NODE_ENV === `development`) {
  const { logger } = require(`redux-logger`);

  middlewares.push(logger);
}

const appStore = composeEnhancers(applyMiddleware(...middlewares), offline(offlineConfig))(createStore)(AppReducer);

if (module.hot) {
  module.hot.accept(() => {
    const nextRootReducer = require('../reducers/index').default;
    appStore.replaceReducer(nextRootReducer);
  });
}
export default appStore;